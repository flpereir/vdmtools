#!/bin/bash

for k in 8999_29Jun23_004658_29Jun23_011220 8999_29Jun23_013851_29Jun23_020425 8999_29Jun23_023227_29Jun23_025502 8999_29Jun23_073830_29Jun23_080352 8999_29Jun23_092415_29Jun23_094738 8999_29Jun23_110004_29Jun23_112226 8999_29Jun23_123257_29Jun23_125514 ; do
    cd $k
    rm *.json
    rm -rf luminometer_data/ corr/
    rm cond/*.pdf cond/*.csv cond/BeamCurrents_8999.json

    for i in PLT HFOC HFET BCM1F ; do
        cd $i
        rm -rf graphs/ plots/
        cd results/

        for j in $(ls) ; do 
            cd $j
            find -type f -not \( -name 'DG_FitResults.csv' -or -name "LumiCalibration_"$i"_DG_8999.csv" \) -delete
            cd ..
        done

        cd ../..
    done
    cd ..
done