from __future__ import annotations
from pathlib import Path
from typing import Tuple, Union, List, Set, Dict
from copy import deepcopy

import re

import pandas as pd

from vdmtools import logging


_logger = logging.get_logger(__name__)


def _get_fit_names() -> Set[str]:
    return {
        "SG", "SGConst", "SGConstS", "DG", "twoMuDG", "DGConst", "DGConstS",
        "GSupGConst", "SimCapSigma_PCCAndVtx", "SuperG", "SuperGConst","Poly2G",
        "Poly4G", "Poly6G", "Poly2GConst", "Poly4GConst", "Poly6GConst", "QGROOT",
        "QGROOTConst", "QG", "QGConst"
    }


def _get_detector_names(read_channels: bool) -> Set[str]:
    detector_ranges = {
        "HFET": range(0),
        "HFOC": range(0),
        "PLT": range(16),
        "BCM1F": range(48),
        "BCM1FUTCA": range(40),
        "pcchd5": range(0),
    }

    detectors = set(detector_ranges.keys())
    if read_channels:
        for detector, channels in detector_ranges.items():
            detectors.update(f"{detector}{chan}" for chan in channels)

    return detectors


def _get_correction_names() -> Set[str]:
    correction_order = [
        "Background", "OrbitDriftSep",
        "OrbitDriftRate", "BeamBeam","DynamicBeta",
        "ResidualOrbitDrift", "LengthScale", "PeakToPeak"
    ]

    # TODO: This is a nasty hack to get this correction name
    #       to be read. It should be added automatically.
    corrections = {"noCorr", "Background_BeamBeam_DynamicBeta", "Background_OrbitDriftSep_OrbitDriftRate_ResidualOrbitDrift"}
    for i in range(len(correction_order)):
        for j in range(i, len(correction_order)):
            corrections.add("_".join(correction_order[i:j+1]))

    return corrections


def _processs_fit_results(fit_results: pd.DataFrame, index: List[str]) -> pd.DataFrame:
    results = fit_results.sort_index().pivot_table(
                            index=index,
                            columns="Type"
                        ).reset_index()

    results.columns = pd.Index([
        f"{name}_{plane}" if plane else name
        for name, plane in results.columns
    ])

    return results


def _process_lumi_results(lumi_results: pd.DataFrame) -> pd.DataFrame:
    results = lumi_results.sort_index().drop(
        columns=["XscanNumber_YscanNumber", "Type"]
    )

    return results


class FWOutputParser:
    """
    Utility class to read the scan results.
    """

    __report_missing_fits: bool = True
    __read_fits: Set[str] = set()
    __report_missing_detectors: bool = True
    __read_detectors: List[str] = []  # It is a list to preserve the order
    __report_missing_corrections: bool = True
    __read_corrections: Set[str] = set()

    @staticmethod
    def read_folder_results(
            path: Path,
            fits: Set[str] = None,
            detectors: Set[str] = None,
            corrections: Set[str] = None,
            read_channels: bool = False
        ) -> Dict[str, pd.DataFrame]:
        """
        Read the existing scan results from the given path.

        Parameters
        ----------
        path : Path
            Path to the scan results directory.
        fits : Set[str], optional
            Set of fits to read, by default None
        detectors : Set[str], optional
            Set of detectors to read, by default None
        corrections : Set[str], optional
            Set of corrections to read, by default None

        Returns
        -------
        Dict[str, pd.DataFrame]
            Dictionary containing the mapping between the fit name and all the
            FitResults and LumiCalibration dataframes for all the detectors 
            and corrections in a single dataframe.
        """
        FWOutputParser.__reset_state()

        if not fits:
            FWOutputParser.__report_missing_fits = False
            fits = _get_fit_names()
            _logger.info("No fits specified. Trying to read all fits.")

        if not detectors:
            FWOutputParser.__report_missing_detectors = False
            detectors = _get_detector_names(read_channels)
            _logger.info("No detectors specified. Trying to read all detectors.")
            if read_channels:
                _logger.info("Reading channels.")

        if not corrections:
            FWOutputParser.__report_missing_corrections = False
            corrections = _get_correction_names()
            _logger.info("No corrections specified. Trying to read all corrections.")

        pre_merged_results: Dict[str, List[Tuple[pd.DataFrame, pd.DataFrame]]] = {}

        # First loop starts in 'analysed_data' directory
        for detector_level_path in path.glob("*"):
            detector = ""
            if detector_level_path.is_dir():
                detector = detector_level_path.stem

            if detector not in detectors:
                continue

            # Second loop starts in 'results' directory
            for correction_level_path in (detector_level_path/"results").glob("*"):
                correction = ""
                if correction_level_path.is_dir():
                    correction = correction_level_path.stem

                if correction not in corrections:
                    continue

                # Third loop starts in '<correction>' directory
                for fit in fits:
                    results = FWOutputParser._read_results(
                        path, fit, detector, correction, True
                    )

                    if not results:
                        if FWOutputParser.__report_missing_fits:
                            _logger.warning(f"{fit} missing for {detector} {correction}")
                        continue

                    if fit not in pre_merged_results:
                        pre_merged_results[fit] = []

                    pre_merged_results[fit].append(results)

        index = ["BCID", "fit", "detector", "correction"]
        merged_results: Dict[str, pd.DataFrame] = {}
        for fit, results in pre_merged_results.items():
            fit_results = pd.concat([result[0] for result in results])
            lumi_results = pd.concat([result[1] for result in results])

            fit_results = _processs_fit_results(fit_results, index)
            lumi_results = _process_lumi_results(lumi_results)

            merged_results[fit] = fit_results.merge(lumi_results, on=index)

        if FWOutputParser.__report_missing_detectors:
            for detector in detectors:
                if detector not in FWOutputParser.__read_detectors:
                    _logger.warning(f"Detector {detector} not found.")

        if FWOutputParser.__report_missing_corrections:
            for correction in corrections:
                if correction not in FWOutputParser.__read_corrections:
                    _logger.warning(f"Correction {correction} not found.")

        return merged_results

    @staticmethod
    def read_results(
        path: Path,
        fit: str,
        detector: str,
        correction: str,
        add_extra_column: bool = False,
    ) -> pd.DataFrame:
        return FWOutputParser._read_results(
            path, fit, detector, correction, add_extra_column, True
        )

    @staticmethod
    def _read_results(
        path: Path,
        fit: str,
        detector: str,
        correction: str,
        add_extra_column: bool = False,
        combine: bool = False,
    ) -> Union[pd.DataFrame, Tuple[pd.DataFrame, pd.DataFrame]]:
        try:
            fill = int(re.search(r"(\d+)*", path.stem).group(1))
        except AttributeError:
            raise ValueError(
                f"Failure parsing fill number from {path.stem}."
                " Path stem must start with fill number."
            )

        fit_results_path = path / detector / "results" / correction / f"{fit}_FitResults.csv"
        lumi_results_path = path / detector / "results" / correction / f"LumiCalibration_{detector}_{fit}_{fill}.csv"

        if not fit_results_path.exists():
            if FWOutputParser.__report_missing_fits:
                _logger.warning(f"FitResults file for {fit} not found at {fit_results_path}.")
            return None

        if not lumi_results_path.exists():
            if FWOutputParser.__report_missing_fits:
                _logger.warning(f"LumiCalibration file for {fit} not found at {lumi_results_path}.")
            return None

        fit_results = pd.read_csv(fit_results_path)
        lumi_results = pd.read_csv(lumi_results_path)

        index = ["BCID"]
        if add_extra_column:
            index.extend(["fit", "detector", "correction"])

            fit_results["fit"] = fit
            fit_results["correction"] = correction
            fit_results["detector"] = detector

            lumi_results["fit"] = fit
            lumi_results["correction"] = correction
            lumi_results["detector"] = detector

        FWOutputParser.__update_read_state(fit, detector, correction)

        if combine:
            fit_results = _processs_fit_results(fit_results, index)
            lumi_results = _process_lumi_results(lumi_results)

            results = fit_results.merge(lumi_results, on=index)
            return results

        return fit_results, lumi_results

    @staticmethod
    def collect_read_state() -> Tuple[Set[str], List[str], Set[str]]:
        """
        Collect the state of the FWOutputParser class.

        Returns
        -------
        Tuple[Set[str], List[str], Set[str]]
            Tuple containing the sets of read fits, detectors, and corrections.
        """
        return (
            deepcopy(FWOutputParser.__read_fits),
            deepcopy(FWOutputParser.__read_detectors),
            deepcopy(FWOutputParser.__read_corrections)
        )

    @staticmethod
    def __update_read_state(fit: str, detector: str, correction: str) -> None:
        FWOutputParser.__read_fits.add(fit)
        FWOutputParser.__read_corrections.add(correction)

        if detector not in FWOutputParser.__read_detectors:
            FWOutputParser.__read_detectors.append(detector)

    @staticmethod
    def __reset_state() -> None:
        FWOutputParser.__report_missing_fits = True
        FWOutputParser.__read_fits.clear()
        FWOutputParser.__report_missing_detectors = True
        FWOutputParser.__read_detectors.clear()
        FWOutputParser.__report_missing_corrections = True
        FWOutputParser.__read_corrections.clear()

