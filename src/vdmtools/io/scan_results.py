from __future__ import annotations
from collections.abc import Iterable as IterableABC
from typing import Iterable, Tuple, Set, List, overload
from pathlib import Path
from datetime import datetime

import re
import json

import pandas as pd

from vdmtools import logging
from vdmtools.utils import DataframeUtils
from vdmtools.io._utils.fw_output_parser import FWOutputParser


_logger = logging.get_logger(__name__)


def read_scan_conditions(path: Path) -> ScanConditions:
    """
    Read the scan conditions from the conditions file in the given path.

    Parameters
    ----------
    path : Path
        The path to the scan results directory.

    Returns
    -------
    ScanConditions
        The scan conditions.

    Raises
    ------
    FileNotFoundError
        If the conditions file is not found.
    KeyError
        If the conditions file is missing any keys.
    ValueError
        If the fill number cannot be parsed from the path.

    Example
    -------
    >>> from pathlib import Path
    >>> from vdmtools.io import read_scan_conditions
    >>> path = Path("/eos/user/v/vdmdata/vdmoutput/www/analysed_data/8999_28Jun23_222504_28Jun23_223127")
    >>> read_scan_conditions(path)
    ScanConditions:
        Fill: 8999
        Run: (369802, 369802)
        Scan names: ('X1', 'Y1')
        Scan types: ('NormalX', 'NormalY')
        Scan time windows:
                First  Scan: 2023-06-28 22:25:04 - 2023-06-28 22:29:56
                Second Scan: 2023-06-28 22:31:27 - 2023-06-28 22:36:22
        Beta star: 19.2
        Angle: 0.0
        Offset: [0.0, 0.0]
        Particle type B1: proton
        Particle type B2: proton
        COM energy B1 (GeV): 6799
        COM energy B2 (GeV): 6799
    """
    try:
        fill = int(re.search(r"(\d+)*", path.stem).group(1))
    except AttributeError:
        raise ValueError(
            f"Failure parsing fill number from {path.stem}."
            " Path stem must start with fill number."
        )

    conditions_path = path / "cond" / f"Scan_{fill}.json"
    if not conditions_path.exists():
        raise FileNotFoundError(
            f"Conditions file for fill {fill} not found at {conditions_path}."
        )

    with open(conditions_path, "r") as file:
        conditions_dict = json.load(file)

    conditions = {"fill": fill}
    try:
        conditions["run"] = tuple(conditions_dict["Run"])
        conditions["scan_names"] = tuple(conditions_dict["ScanNames"])
        conditions["scan_types"] = tuple(conditions_dict["ScanTypes"])
        conditions["scan_time_windows"] = tuple(
            tuple(datetime.fromtimestamp(t) for t in tw)
            for tw in conditions_dict["ScanTimeWindows"]
        )
        conditions["beta_star"] = conditions_dict["BetaStar"]
        conditions["angle"] = conditions_dict["Angle"]
        conditions["offset"] = conditions_dict["Offset"]
        conditions["ptype_b1"] = conditions_dict["ParticleTypeB1"]
        conditions["ptype_b2"] = conditions_dict["ParticleTypeB2"]
        conditions["energy_b1"] = conditions_dict["EnergyB1"]
        conditions["energy_b2"] = conditions_dict["EnergyB2"]
    except KeyError as e:
        raise KeyError(
            f"Conditions file for fill {fill} at {conditions_path} is missing key {e}."
            f"If this error is to be ignored, add a default value for {e} to the conditions file."
        )

    _logger.info("Read scan conditions from %s.", path.stem)

    scan_conditions = ScanConditions(**conditions)
    _logger.debug(str(scan_conditions))

    return scan_conditions


@overload
def read_one_or_many(paths: Iterable[Path], scan_names: Iterable[str], **kwargs) -> List[ScanResults]:
    ...
@overload
def read_one_or_many(paths: Path, scan_names: str, **kwargs) -> ScanResults:
    ...
def read_one_or_many(paths, scan_names=None, **kwargs):
    """Read one many ScanResults from the given paths.

    Parameters
    ----------
    paths : List[Path] or Path
        The paths or path to the scan results directories.
    scan_names : Iterable[str] or str
        The scan names **in the order they were performed**. If not given, 
        the scan names will be empty strings.

    Returns
    -------
    List[ScanResults] or ScanResults
        The single or many ScanResults. If many, they are sorted by scan start time.

    Raises
    ------
    TypeError
        If the types of `paths` and `scan_names` are not valid.

    Examples
    --------
    **Read one ScanResults**:

    >>> from pathlib import Path
    >>> from vdmtools.io import read_one_or_many
    >>> path = Path("/eos/user/v/vdmdata/vdmoutput/www/analysed_data/8999_28Jun23_230143_28Jun23_232943")
    >>> scan_results = read_one_or_many(path)
    >>> scan_results
    ScanResults from: /eos/user/v/vdmdata/vdmoutput/www/analysed_data/8999_28Jun23_230143_28Jun23_232943
    Scan name: VdM1
    Scan ID: 8999_28Jun23_230143_28Jun23_232943
    ScanConditions:
            Fill: 8999
            Run: (369802, 369802)
            Scan names: ('X1', 'Y1')
            Scan types: ('NormalX', 'NormalY')
            Scan time windows:
                    First  Scan: 2023-06-28 22:25:04 - 2023-06-28 22:29:56
                    Second Scan: 2023-06-28 22:31:27 - 2023-06-28 22:36:22
            Beta star: 19.2
            Angle: 0.0
            Offset: [0.0, 0.0]
            Particle type B1: proton
            Particle type B2: proton
            COM energy B1 (GeV): 6799
            COM energy B2 (GeV): 6799
    Results:
            Fits: {'SGConst', 'SG', 'DG'}
            Detectors: ['BCM1F', 'BCM1FUTCA', 'HFET', 'HFOC', 'PLT']
            Corrections: {'Background', 'noCorr', 'BeamBeam_DynamicBeta'}

    **Read many ScanResults**:

    >>> from pathlib import Path
    >>> from vdmtools.io import read_one_or_many
    >>> paths = [
    ...     Path("/eos/user/v/vdmdata/vdmoutput/www/analysed_data/8999_28Jun23_230143_28Jun23_232943"),
    ...     Path("/eos/user/v/vdmdata/vdmoutput/www/analysed_data/8999_29Jun23_004658_29Jun23_011220"),
    ... ]
    >>> names = ["VdM1", "VdM2"]
    >>> scan_results = read_one_or_many(paths, names)
    >>> scan_results[0]
    ScanResults from: /eos/user/v/vdmdata/vdmoutput/www/analysed_data/8999_28Jun23_230143_28Jun23_232943
    Scan name: VdM1
    Scan ID: 8999_28Jun23_230143_28Jun23_232943
    ScanConditions:
            Fill: 8999
            Run: (369802, 369802)
            Scan names: ('X1', 'Y1')
            Scan types: ('NormalX', 'NormalY')
            Scan time windows:
                    First  Scan: 2023-06-28 23:01:43 - 2023-06-28 23:17:35
                    Second Scan: 2023-06-28 23:29:43 - 2023-06-28 23:45:35
            Beta star: 19.2
            Angle: 0.0
            Offset: [0.0, 0.0]
            Particle type B1: proton
            Particle type B2: proton
            COM energy B1 (GeV): 6799
            COM energy B2 (GeV): 6799
    Results:
            Fits: {'SG', 'SGConst', 'DG'}
            Detectors: ['BCM1F', 'BCM1FUTCA', 'HFET', 'HFOC', 'PLT']
            Corrections: {'BeamBeam_DynamicBeta', 'noCorr', 'Background'}
    >>> scan_results[1]
    ScanResults from: /eos/user/v/vdmdata/vdmoutput/www/analysed_data/8999_29Jun23_004658_29Jun23_011220
    Scan name: VdM2
    Scan ID: 8999_29Jun23_004658_29Jun23_011220
    ScanConditions:
            Fill: 8999
            Run: (369802, 369802)
            Scan names: ('X1', 'Y1')
            Scan types: ('NormalX', 'NormalY')
            Scan time windows:
                    First  Scan: 2023-06-29 00:46:58 - 2023-06-29 01:04:49
                    Second Scan: 2023-06-29 01:12:20 - 2023-06-29 01:30:20
            Beta star: 19.2
            Angle: 0.0
            Offset: [0.0, 0.0]
            Particle type B1: proton
            Particle type B2: proton
            COM energy B1 (GeV): 6799
            COM energy B2 (GeV): 6799
    Results:
            Fits: {'SG', 'SGConst', 'DG'}
            Detectors: ['BCM1F', 'BCM1FUTCA', 'HFET', 'HFOC', 'PLT']
            Corrections: {'BeamBeam_DynamicBeta', 'noCorr', 'Background'}
    """
    if isinstance(paths, Path) or isinstance(paths, str):
        scan_name = scan_names or ""
        return ScanResults(path=paths, scan_name=scan_name, **kwargs)
    elif isinstance(paths, IterableABC):
        def get_start_time(scan: ScanResults) -> datetime:
            return scan.conditions.scan_time_windows[0][0]

        # Sort by scan start time first. 
        scans = sorted(
            [ScanResults(path=path, **kwargs) for path in paths],
            key=get_start_time
        )

        # Assign scan names if given so that they are in the same order as the paths.
        scan_names = scan_names or ["" for _ in paths]
        for scan, scan_name in zip(scans, scan_names):
            scan.scan_name = scan_name

        return scans
    else:
        raise TypeError(f"Invalid types for paths and scan_names: {type(paths)}, {type(scan_names)}.")


class ScanConditions:
    """Represents the conditions a scan was performed under.

    Parameters
    ----------
    fill : int
        The fill number.
    run : Tuple[int, int]
        The run numbers.
    scan_names : Tuple[str, str]
        The scan names. E.g. ``("X1", "Y1")``.
    scan_types : Tuple[str, str]
        The scan types. E.g. ``("NormalX", "NormalY")``.
    scan_time_windows : Tuple[Tuple[datetime, datetime], Tuple[datetime, datetime]]
        The scan time windows. E.g. ``((<start>, <end>), (<start>, <end>))``.
    beta_star : float
        The beta star value.
    angle : float
        The crossing angle.
    offset : flaot
        The offset
    ptype_b1 : str
        The particle type for beam 1.
    ptype_b2 : str
        The particle type for beam 2.
    energy_b1 : float
        The COM energy for beam 1 in GeV.
    energy_b2 : float
        The COM energy for beam 2 in GeV.
    """
    def __init__(
            self,
            fill: int,
            run: Tuple[int, int],
            scan_names: Tuple[str, str],
            scan_types: Tuple[str, str],
            scan_time_windows: Tuple[Tuple[datetime, datetime], Tuple[datetime, datetime]],
            beta_star: float,
            angle: float,
            offset: float,
            ptype_b1: str,
            ptype_b2: str,
            energy_b1: float,
            energy_b2: float,
        ) -> None:
        self.fill = fill
        self.run = run
        self.scan_names = scan_names
        self.scan_types = scan_types
        self.scan_time_windows = scan_time_windows
        self.beta_star = beta_star
        self.angle = angle
        self.offset = offset
        self.ptype_b1 = ptype_b1
        self.ptype_b2 = ptype_b2
        self.energy_b1 = energy_b1
        self.energy_b2 = energy_b2

    @property
    def energy(self) -> float:
        """
        Returns the center of mass energy of the scan in GeV.
        """
        return self.energy_b1 + self.energy_b2

    def __repr__(self) -> str:
        result = "ScanConditions:\n"
        result += f"\tFill: {self.fill}\n"
        result += f"\tRun: {self.run}\n"
        result += f"\tScan names: {self.scan_names}\n"
        result += f"\tScan types: {self.scan_types}\n"
        result += f"\tScan time windows:\n"
        result += f"\t\tFirst  Scan: {self.scan_time_windows[0][0]} - {self.scan_time_windows[0][1]}\n"
        result += f"\t\tSecond Scan: {self.scan_time_windows[1][0]} - {self.scan_time_windows[1][1]}\n"
        result += f"\tBeta star: {self.beta_star}\n"
        result += f"\tAngle: {self.angle}\n"
        result += f"\tOffset: {self.offset}\n"
        result += f"\tParticle type B1: {self.ptype_b1}\n"
        result += f"\tParticle type B2: {self.ptype_b2}\n"
        result += f"\tCOM energy B1 (GeV): {self.energy_b1}\n"
        result += f"\tCOM energy B2 (GeV): {self.energy_b2}\n"
        return result

    def __str__(self) -> str:
        return self.__repr__()


class ScanResults:
    """Container for the results of a scan.

    This class provides a convenient way to access the results of a scan given
    the path to the scan results directory. It also reads the scan conditions
    from the conditions file which is located in the ``cond`` directory of the
    scan results directory.

    .. tip::
        Consider using this class when implementing a new :py:class:`~vdmtools.plotting.IStrategyPlugin`.

    Parameters
    ----------
    path : Path
        The path to the scan results directory.
    scan_name : str, default: ""
        The name of the scan. E.g. ``VdM1``.
    fits : Set[str], default: set()
        The names of the fits to read. If empty, all fits are read.
    detectors : List[str], default: list()
        The names of the detectors to read. If empty, all detectors are read.
    corrections : Set[str], default: set()
        The names of the corrections to read. If empty, all corrections are read.
    read_channels : bool, default: False
        If ``True``, also reads the results from detector channels.

    Attributes
    ----------
    results : Dict[str, pd.DataFrame]
        The underlying results. It's a dictionary where the keys are the names of the fits
        (like ``SG``, ``DG``, etc.) and the values are pandas DataFrames containing the results.

        The dataframes all contain the same columns apart from the fit specific ones (``SG`` fits
        do not require ``Frac`` or ``FracErr`` since there is only one gaussian). All the columns
        that were present in the original result files are kept besides the ``XscanNumber_YscanNumber``
        and ``Type`` columns. All other columns were suffixed with ``_<PLANE>``, where ``<PLANE>`` is
        either ``X`` or ``Y`` depending on the plane in question. There were also added the columns
        ``fit``, ``detector`` and ``correction`` which contain the name of the fit, detector and 
        correction in that row.

        .. table:: Summary of the columns present in the results.
            :align: center

            +------------------+------------------------------------------------------+
            | Column           | Description                                          |
            +==================+======================================================+
            | ``fit``          | The fit for that row.                                |
            +------------------+------------------------------------------------------+
            | ``detector``     | The detector for that row.                           |
            +------------------+------------------------------------------------------+
            | ``correction``   | The correction for that row.                         |
            +------------------+------------------------------------------------------+
            | ``<NAME>_X``     | Quantity ``<NAME>`` for the in the X plane.          |
            +------------------+------------------------------------------------------+
            | ``<NAME>_Y``     | Quantity ``<NAME>`` for the in the Y plane.          |
            +------------------+------------------------------------------------------+
            | ``<NAME>Err_X``  | Error on quantity ``<NAME>`` for the in the X plane. |
            +------------------+------------------------------------------------------+
            | ``<NAME>Err_Y``  | Error on quantity ``<NAME>`` for the in the Y plane. |
            +------------------+------------------------------------------------------+

        .. note:: 
            Some quantities are plane independent. In this case they are not suffixed with ``_X`` or ``_Y``.

        Some examples of how to access the results:

        >>> results = scan_results.results
        >>> results.keys()
        dict_keys(['SG', 'DG', 'SGConst'])
        >>> results["SG"]
                  BCID fit   detector            correction  ...        xsec   xsecErr      SBIL   SBILErr
            0      103  SG      BCM1F            Background  ...  135.803567  0.635974  0.069527  0.000388
            1      103  SG      BCM1F  BeamBeam_DynamicBeta  ...  138.506193  0.659651  0.067518  0.000382
            2      103  SG      BCM1F                noCorr  ...  137.865789  0.659816  0.068165  0.000387
            3      103  SG  BCM1FUTCA            Background  ...  127.995151  0.685705  0.068811  0.000435
            4      103  SG  BCM1FUTCA                noCorr  ...  129.702695  0.688118  0.067679  0.000424
            ...    ...  ..        ...                   ...  ...         ...       ...       ...       ...
            1899  3144  SG       HFOC  BeamBeam_DynamicBeta  ...  946.752519  1.674816  0.076859  0.000163
            1900  3144  SG       HFOC                noCorr  ...  941.786213  1.675058  0.077677  0.000165
            1901  3144  SG        PLT            Background  ...  299.420665  1.768880  0.078534  0.000544
            1902  3144  SG        PLT  BeamBeam_DynamicBeta  ...  303.675925  1.779477  0.076760  0.000528
            1903  3144  SG        PLT                noCorr  ...  302.015262  1.780534  0.077613  0.000537


    conditions : ScanConditions
        The conditions of the scan.
    scan_id : str
        The ID of the scan. E.g.``8999_28Jun23_230143_28Jun23_232943``
    """
    def __init__( # TODO: reduced chi2 is a quantity that should be added to the results. Probably optionally.
            self,
            path: Path,
            scan_name: str = "",
            fits: Set[str] = None,
            detectors: List[str] = None,
            corrections: Set[str] = None,
            read_channels: bool = False,
        ) -> None:
        self.path = path if isinstance(path, Path) else Path(path)
        self.scan_name = scan_name
        self.fits = fits or set()
        self.detectors = detectors or list()
        self.corrections = corrections or set()
        self.read_channels = read_channels

        # Check if the types of `self.path`, `self.fits`, `self.detectors` and `self.corrections`
        # are correct. If not, convert them to the correct type.
        self._correct_types()

        self.scan_id = self.path.stem

        self.conditions = read_scan_conditions(self.path)
        self.results = FWOutputParser.read_folder_results(
            path=self.path,
            fits=self.fits,
            detectors=set(self.detectors), # convert to set since it is required by read_folder_results
            corrections=self.corrections,
            read_channels=self.read_channels,
        )
        self.fits, self.detectors, self.corrections = FWOutputParser.collect_read_state()


    def filter_results_by(
            self,
            fit: str,
            detector: str,
            correction: str,
            fit_status: str = "as_is",
            cov_status: str = "as_is",
        ) -> pd.DataFrame:
        """
        Return a DataFrame containing the results form the filter criteria.

        Parameters
        ----------
        fit : str
            The name of the fit to filter by.
        detector : str
            The name of the detector to filter by.
        correction : str
            The name of the correction to filter by.
        fit_status : str, default: "as_is"
            The status of the fit to filter by. Can be one of "as_is", "good", "bad"
        cov_status : str, default: "as_is"
            The status of the covariance matrix to filter by. Can be one of "as_is", "good", "bad"

        Returns
        -------
        pd.DataFrame
            The filtered results.

        Raises
        ------
        ValueError
            If the filtering returns an empty DataFrame.

        Examples
        --------
        **Filter for a specific fit, detector and correction**:

        >>> data = scan_results.filter_results_by("SG", "BCM1F", "Background")
        >>> data.head()
                BCID fit detector            correction  ...        xsec   xsecErr      SBIL   SBILErr
            0    103  SG    BCM1F  BeamBeam_DynamicBeta  ...  140.960688  0.883231  0.065656  0.000490
            8    124  SG    BCM1F  BeamBeam_DynamicBeta  ...  140.219116  0.828196  0.072860  0.000513
            16   145  SG    BCM1F  BeamBeam_DynamicBeta  ...  140.614667  0.917215  0.058912  0.000450
            24   166  SG    BCM1F  BeamBeam_DynamicBeta  ...  140.183518  0.815876  0.069397  0.000472
            32   187  SG    BCM1F  BeamBeam_DynamicBeta  ...  139.707410  0.882703  0.059622  0.000443

        **Filter also by ``fit status``**:

        >>> data = scan_results.filter_results_by("SG", "BCM1F", "Background", fit_status="good")
        >>> data["fitStatus_X"].unique()
            array([0])
        """
        data = self.results[fit].query(
            f"detector == '{detector}' and correction == '{correction}'"
        )
        if data.empty:
            raise ValueError(
                "Filtering returned an empty DataFrame."
                f" Check that the fit '{fit}', detector '{detector}' and correction '{correction}'"
                f" are present in the results in the {self.path} directory."
            )

        fit_status_dict = {
            "as_is": -1,
            "good": 0,
            "bad": 4
        }
        if fit_status_dict[fit_status] != -1:
            _logger.debug("Filtering by fit status: %s", fit_status)
            _logger.debug("Fit status is: %s", fit_status_dict[fit_status])
            data = DataframeUtils.filter_by_fit_status(data, fit_status_dict[fit_status])

        cov_status_dict = {
            "as_is": -1,
            "good": 3,
            "bad": 2
        }
        if cov_status_dict[cov_status] != -1:
            _logger.debug(f"Filtering by cov status: {cov_status}")
            _logger.debug(f"Cov status is: {cov_status_dict[cov_status]}")
            data = DataframeUtils.filter_by_cov_status(data, cov_status_dict[cov_status])

        return data

    def __repr__(self) -> str:
        result = f"ScanResults from: {self.path}\n"
        result += f"Scan name: {self.scan_name}\n"
        result += f"Scan ID: {self.scan_id}\n"
        result += f"{self.conditions}\n"
        result += "Results:\n"
        result += f"\tFits: {self.fits}\n"
        result += f"\tDetectors: {self.detectors}\n"
        result += f"\tCorrections: {self.corrections}\n"
        return result

    def __str__(self) -> str:
        return self.__repr__()

    def _correct_types(self) -> None:
        if not isinstance(self.path, Path):
            _logger.warning("Path is not a Path object. Converting to Path.")
            self.path = Path(self.path)

        if not isinstance(self.fits, set):
            _logger.warning("Fits is not a set. Converting to set.")
            self.fits = set(self.fits)

        if not isinstance(self.detectors, list):
            _logger.warning("Detectors is not a list. Converting to list.")
            self.detectors = list(self.detectors)

        if not isinstance(self.corrections, set):
            _logger.warning("Corrections is not a set. Converting to set.")
            self.corrections = set(self.corrections)
