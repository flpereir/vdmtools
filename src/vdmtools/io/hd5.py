# Code heavily inpired on this repository: https://gitlab.cern.ch/adelanno/lumi_stuff/-/tree/master/hd5?ref_type=heads
from typing import Any, List, Dict, Optional, Tuple
from pathlib import Path

import tables
import pandas as pd

from vdmtools import logging


SCALAR_SHAPE = ()
logger = logging.get_logger(__name__)


class Lumitable(tables.IsDescription):
	fillnum = tables.UInt32Col(shape=(), dflt=0, pos=0)
	runnum = tables.UInt32Col(shape=(), dflt=0, pos=1)
	lsnum = tables.UInt32Col(shape=(), dflt=0, pos=2)
	nbnum = tables.UInt32Col(shape=(), dflt=0, pos=3)
	timestampsec = tables.UInt32Col(shape=(), dflt=0, pos=4)
	timestampmsec = tables.UInt32Col(shape=(), dflt=0, pos=5)
	totsize = tables.UInt32Col(shape=(), dflt=0, pos=6)
	publishnnb = tables.UInt8Col(shape=(), dflt=0, pos=7)
	datasourceid = tables.UInt8Col(shape=(), dflt=0, pos=8)
	algoid = tables.UInt8Col(shape=(), dflt=0, pos=9)
	channelid = tables.UInt8Col(shape=(), dflt=0, pos=10)
	payloadtype = tables.UInt8Col(shape=(), dflt=0, pos=11)
	calibtag = tables.StringCol(itemsize=32, shape=(), dflt='', pos=12)
	avgraw = tables.Float32Col(shape=(), dflt=0.0, pos=13)
	avg = tables.Float32Col(shape=(), dflt=0.0, pos=14)
	bxraw = tables.Float32Col(shape=(3564,), dflt=0.0, pos=15)
	bx = tables.Float32Col(shape=(3564,), dflt=0.0, pos=16)
	maskhigh = tables.UInt32Col(shape=(), dflt=0, pos=17)
	masklow = tables.UInt32Col(shape=(), dflt=0, pos=18)


class BeamTable(tables.IsDescription):
    fillnum = tables.UInt32Col(shape=(), dflt=0, pos=0)
    runnum = tables.UInt32Col(shape=(), dflt=0, pos=1)
    lsnum = tables.UInt32Col(shape=(), dflt=0, pos=2)
    nbnum = tables.UInt32Col(shape=(), dflt=0, pos=3)
    timestampsec = tables.UInt32Col(shape=(), dflt=0, pos=4)
    timestampmsec = tables.UInt32Col(shape=(), dflt=0, pos=5)
    totsize = tables.UInt32Col(shape=(), dflt=0, pos=6)
    publishnnb = tables.UInt8Col(shape=(), dflt=0, pos=7)
    datasourceid = tables.UInt8Col(shape=(), dflt=0, pos=8)
    algoid = tables.UInt8Col(shape=(), dflt=0, pos=9)
    channelid = tables.UInt8Col(shape=(), dflt=0, pos=10)
    payloadtype = tables.UInt8Col(shape=(), dflt=0, pos=11)
    status = tables.StringCol(itemsize=28, shape=(), dflt=b'', pos=12)
    egev = tables.Float32Col(shape=(), dflt=0.0, pos=13)
    targetegev = tables.UInt16Col(shape=(), dflt=0, pos=14)
    bxconfig1 = tables.UInt8Col(shape=(3564,), dflt=0, pos=15)
    bxconfig2 = tables.UInt8Col(shape=(3564,), dflt=0, pos=16)
    intensity1 = tables.Float32Col(shape=(), dflt=0.0, pos=17)
    intensity2 = tables.Float32Col(shape=(), dflt=0.0, pos=18)
    bxintensity1 = tables.Float32Col(shape=(3564,), dflt=0.0, pos=19)
    bxintensity2 = tables.Float32Col(shape=(3564,), dflt=0.0, pos=20)
    machinemode = tables.StringCol(itemsize=20, shape=(), dflt=b'', pos=21)
    fillscheme = tables.StringCol(itemsize=64, shape=(), dflt=b'', pos=22)
    ncollidable = tables.Int32Col(shape=(), dflt=0, pos=23)
    collidable = tables.UInt8Col(shape=(3564,), dflt=0, pos=24)


def pandas_to_hd5(
        data: pd.DataFrame,
        path: Path,
        node: str,
        mode: str,
        table_description: tables.IsDescription,
        complevel: int = 9,
        complib: str = "blosc",
        chunkshape: Tuple[int, ...] = (100,)
    ) -> None:
    """Save a pandas dataframe to an hd5 file.

    Parameters
    ----------
    data: pd.DataFrame
        The dataframe to save as hd5 file.
    path: Path
        The path where to save the data.
    node: str
        The name of the node where to save the data.
    mode: str
        In which mode to open the file.
        Supported are:
        * `w`: Open the file in write mode. Deleting the content of the file if it existed.
          Otherwise creates the file.
        * `r`: Open the file in read mode. File must exist. Only read permissions.
        * `a`: Open the file in append mode. Read and write permissions.

    table_description: tables.IsDescription
        An tables compatible description of the hd5 file table to be created.
    complevel: int, default 9
        The compression level.
    complib: str, default "blosc"
        The compression library to use.
    chunkshape: Tuple[int, ...], default (100,)
        The chunk shape to use when saving.
    """
    filters = tables.Filters(complevel=complevel, complib=complib)
    out = tables.open_file(path, mode=mode)
    out_table = out.create_table("/", node, table_description, filters=filters, chunkshape=chunkshape)
    record = data.to_records(index=False).tolist()
    out_table.append(record)
    out_table.flush()
    out.close()


def _get_data_dict(table_node: tables.Table, columns: Optional[List[str]] = None) -> Dict[str, Any]:
    return {
        column: (
            table_node.col(column) if
            table_node.coldescrs[column].shape == SCALAR_SHAPE else
            table_node.col(column).tolist()
        ) for column in columns
    } if columns is not None else table_node.read().tolist()


def hd5_to_pandas(path: Path, node: str, columns: Optional[List[str]] = None) -> pd.DataFrame:
    """Reads a specific node from a hd5 file and returns it as a pandas DataFrame.

    Any hd5 column that is not scalar will be kept as a single column. To expand
    non-scalar columns into multiple columns, use the :func:`~vdmtools.io.hd5.hd5_to_pandas_expanded`
    function.

    Parameters
    ----------
    path : Path
        Path to the hd5 file.
    node : str
        Name of the node to read.
    columns: Optional[List[str]], default None
        An optional list of columns to read from the hd5 file. Reads all if not provided.

    Returns
    -------
    pd.DataFrame
        DataFrame containing the data from the node.
    """
    with tables.open_file(path) as table:
        table_node = table.get_node("/", node)

        # Assert so linter helps us
        assert isinstance(table_node, tables.Table)

        data = _get_data_dict(table_node, columns)

        return pd.DataFrame(data, columns=columns if columns is not None else table_node.colnames)


def hd5_to_pandas_expanded(path: Path, node: str, columns: Optional[List[str]] = None) -> pd.DataFrame:
    """Reads a specific node from a hd5 file and returns it as a pandas DataFrame.

    Any hd5 column that is not scalar will be expanded into multiple columns. The
    name of the new columns will be the name of the original column with a suffix
    of the index of the element in the array. To keep non-scalar columns in a single
    column, use the :func:`~vdmtools.io.hd5.hd5_to_pandas` function.

    Parameters
    ----------
    path : Path
        Path to the hd5 file.
    node : str
        Name of the node to read.
    columns: Optional[List[str]], default None
        An optional list of columns to read from the hd5 file. Reads all if not provided.

    Returns
    -------
    pd.DataFrame
        DataFrame containing the data from the node.
    """
    with tables.open_file(path) as table:
        table_node = table.get_node("/", node)

        # Assert so linter helps us
        assert isinstance(table_node, tables.Table)

        columns = columns or table_node.colnames

        # Get the shape of each column
        col = pd.Series({
            col: descr.shape
            for col, descr in table_node.coldescrs.items()
            if col in columns
        })

        # Get the scalar columns
        data = pd.DataFrame(table_node[:][col[col == SCALAR_SHAPE].index])

        # Get the array columns
        bx_data = [
            pd.DataFrame(table_node[:][col]).add_prefix(f'{col}_')  # Add prefix to avoid name clashes
            for col in col[col != SCALAR_SHAPE].index
        ]

        # Concatenate everything
        return pd.concat([data] + bx_data, axis=1)


def read_entire_folder(path: Path, node: str, columns: Optional[List[str]] = None, expanded: bool = False) -> pd.DataFrame:
    """Reads all the hd5 files in a folder and returns them as a pandas DataFrame.

    Parameters
    ----------

    path : Path
        Path to the folder containing the hd5 files.
    node : str
        Name of the node to read.
    columns: Optional[List[str]], default None
        An optional list of columns to read from the hd5 file. Reads all if not provided.
    expanded : bool
        Whether to expand non-scalar columns into multiple columns. See
        :func:`~vdmtools.io.hd5.hd5_to_pandas_expanded` for more details.

    Returns
    -------
    pd.DataFrame
        DataFrame containing the data from the node.
    """
    if not isinstance(path, Path):
        path = Path(path)

    read_func = hd5_to_pandas
    if expanded:
        read_func = hd5_to_pandas_expanded

    dfs = []
    for file in path.iterdir():
        if file.suffix == ".hd5":
            dfs.append(read_func(file, node, columns=columns))
    return pd.concat(dfs, ignore_index=True)

