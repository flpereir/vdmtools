from typing import Optional
from logging import CRITICAL
from logging import DEBUG
from logging import ERROR
from logging import FATAL
from logging import INFO
from logging import WARN
from logging import WARNING

import sys
import logging
import threading


__all__ = [
    "CRITICAL",
    "DEBUG",
    "ERROR",
    "FATAL",
    "INFO",
    "WARN",
    "WARNING",
]

_lock: threading.Lock = threading.Lock()
_default_handler: Optional[logging.Handler] = None


class ANSIColorFormatter(logging.Formatter):
    COLORS = {
        'WARNING': '\033[93m',  # Yellow
        'INFO': '\033[92m',     # Green
        'DEBUG': '\033[94m',    # Blue
        'CRITICAL': '\033[91m', # Red
        'ERROR': '\033[91m',    # Red
    }

    RESET = '\033[0m'  # Reset to default

    def __init__(self, fmt, datefmt=None):
        super().__init__(fmt, datefmt)
        self.fmt = fmt

    def format(self, record: logging.LogRecord):
        original_format = self._fmt

        # Set color and reset codes in the record
        record.log_color = self.COLORS.get(record.levelname, self.RESET)
        record.reset = self.RESET

        # Format the message
        formatted_message = super().format(record)

        # Restore the original format
        self._fmt = original_format

        return formatted_message


def create_default_formatter() -> logging.Formatter:
    """Create a default formatter of log messages.

    This function is not supposed to be directly accessed by library users.
    """
    header = "[%(levelname)8s %(name)s ]"
    message = "%(message)s"
    if sys.stdout.isatty():
        return ANSIColorFormatter(f"%(log_color)s{header}%(reset)s {message}",)
    else:
        return logging.Formatter(f"{header} {message}")


def _get_library_name() -> str:
    return __name__.split(".")[0]


def _get_library_root_logger() -> logging.Logger:
    return logging.getLogger(_get_library_name())


def _configure_library_root_logger() -> None:
    global _default_handler

    with _lock:
        if _default_handler:
            # This library has already configured the library root logger.
            return
        _default_handler = logging.StreamHandler(sys.stdout)
        _default_handler.setFormatter(create_default_formatter())

        # Apply our default configuration to the library root logger.
        library_root_logger = _get_library_root_logger()
        library_root_logger.addHandler(_default_handler)
        library_root_logger.setLevel(logging.INFO)
        library_root_logger.propagate = False


def _reset_library_root_logger() -> None:
    global _default_handler

    with _lock:
        if not _default_handler:
            return

        library_root_logger: logging.Logger = _get_library_root_logger()
        library_root_logger.removeHandler(_default_handler)
        library_root_logger.setLevel(logging.NOTSET)
        _default_handler = None


def get_logger(name: str) -> logging.Logger:
    """Return a logger with the specified name.

    This function is not supposed to be directly accessed by library users.
    """

    _configure_library_root_logger()
    return logging.getLogger(name)


def get_verbosity() -> int:
    """
    Return the current level for the vdmtools' root logger.

    Returns
    -------
    int
        Logging level, e.g., ``vdmtools.logging.DEBUG`` and ``vdmtools.logging.INFO``.
    """

    _configure_library_root_logger()
    return _get_library_root_logger().getEffectiveLevel()


def verbosity_from_str(verbosity: str) -> int:
    """Set the level for the vdmtools' root logger.

    Parameters
    ----------
    verbosity : str
        One of "CRITICAL", "ERROR", "WARNING", "INFO" and "DEBUG".
    """

    verbosity = getattr(logging, verbosity, None)
    if not isinstance(verbosity, int):
        raise ValueError(f"Invalid log level: {verbosity}")

    return verbosity


def set_verbosity(verbosity: int) -> None:
    """Set the level for the vdmtools' root logger.

    Parameters
    ----------
    verbosity : int
        Logging level, e.g., ``vdmtools.logging.DEBUG`` and ``vdmtools.logging.INFO``.

    Note
    ----
    The follwing looging levels are available.

    * ``vdmtools.logging.CRITICAL``
    * ``vdmtools.logging.ERROR``
    * ``vdmtools.logging.WARNING``
    * ``vdmtools.logging.INFO``
    * ``vdmtools.logging.DEBUG``
    """

    _configure_library_root_logger()
    _get_library_root_logger().setLevel(verbosity)


def disable_default_handler() -> None:
    """Disable the default handler of the vdmtools' root logger."""

    _configure_library_root_logger()

    assert _default_handler is not None
    _get_library_root_logger().removeHandler(_default_handler)


def enable_default_handler() -> None:
    """Enable the default handler of the vdmtools' root logger."""

    _configure_library_root_logger()

    assert _default_handler is not None
    _get_library_root_logger().addHandler(_default_handler)


def disable_propagation() -> None:
    """
    Disable propagation of the library log outputs.

    Note
    ----
    Log propagation is disabled by default. You only need to use this function
    to stop log propagation when you use :func:`~vdmtools.logging.enable_propagation()`.
    """

    _configure_library_root_logger()
    _get_library_root_logger().propagate = False


def enable_propagation() -> None:
    """
    Enable propagation of the library log outputs.

    Please disable the vdmtools' default handler to prevent double logging if the root logger has
    been configured.
    """

    _configure_library_root_logger()
    _get_library_root_logger().propagate = True