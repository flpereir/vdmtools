from typing import Dict
from pathlib import Path

import json
import argparse

from vdmtools import logging
from vdmtools.utils.plugins import load_module
from vdmtools.plotting import IStrategyPlugin, VdMPlotter
from .. import _INTERNAL_PLUGINS, cli_standard


_logger = logging.get_logger(__name__)


def configure_parser(subparser: argparse._SubParsersAction) -> None:
    parser = subparser.add_parser(
        "run-config",
        help="Run the plotting engine with a config file",
    )

    parser.add_argument(
        "config",
        type=str,
        help="Path to the config file",
    )

    parser.add_argument(
        "-p", "--plugins",
        help="Path to the plugins directory",
        default=_INTERNAL_PLUGINS,
        metavar=""
    )


@cli_standard
def run(args: argparse.Namespace) -> None:
    # Confirm the config file is a python file
    config_path = Path(args.config)
    if config_path.suffix != ".py":
        raise ValueError("Config file must be a python file.")

    plugins_path = Path(args.plugins)
    if plugins_path == _INTERNAL_PLUGINS:
        _logger.info("No external plugins directory specified. Using internal plugins.")
    else:
        # Confirm the plugins directory exists
        if not plugins_path.exists():
            raise FileNotFoundError(f"Plugins directory not found: {plugins_path}")

    plotting_engine(config_path, plugins_path)

def _load_plugins(plugins, plugins_path):
    registered_plugins: Dict[str, type[IStrategyPlugin]]= {}

    for plugin in plugins:
        plugin_path = Path(plugins_path)/plugin

        try:
            with open(plugin_path/"plugin.json", "r") as f:
                plugin_dict = json.load(f)

            plugin_runtime = plugin_dict["runtime"]
        except FileNotFoundError:
            _logger.warning(f"Plugin {plugin} not found.")
            continue
        except KeyError:
            _logger.warning(f"Plugin {plugin} missing `runtime` key.")
            continue

        try:
            module = load_module(plugin_path/f"{plugin_runtime}.py")
        except FileNotFoundError:
            _logger.warning(f"Plugin {plugin} runtime module not found.")
            continue


        try:
            last_loaded = IStrategyPlugin.registered_strategies[-1]
            if last_loaded.__module__ != module.__name__:
                _logger.warning(f"Error loading plugin {plugin}.")
                continue
        except IndexError:
            _logger.warning(f"Error loading plugin {plugin}.")
            continue

        _logger.info(f"Loaded plugin {plugin}.")
        registered_plugins[last_loaded.__name__] = last_loaded

    IStrategyPlugin.registered_strategies.clear()

    return registered_plugins


def _run_plots(plots, registered_plugins):
    required_keys = ["strategy_name", "strategy_args", "context", "data"]

    _logger.info(f"Registered plot strategies: {', '.join(registered_plugins.keys())}")

    for plot in plots:
        # Check if all required keys are in the plot
        if not all(key in plot for key in required_keys):
            missing_keys = [key for key in required_keys if key not in plot]
            _logger.warning(f"Plot missing required keys: {', '.join(missing_keys)}. Skipping.")
            continue

        if plot["strategy_name"] not in registered_plugins:
            _logger.warning(f"Plot strategy {plot['strategy_name']} not loaded from plugin.")
            _logger.warning(f"Make sure the required plugin exists")
            continue

        strategy = registered_plugins[plot["strategy_name"]](**plot["strategy_args"])
        plotter = VdMPlotter(strategy)

        _logger.info(f"Running Strategy {plot['strategy_name']}")
        plotter.run(plot["data"], plot["context"])


def _plotting_engine(plots, plugins, plugins_path):
    registered_plugins = _load_plugins(plugins, plugins_path)

    _run_plots(plots, registered_plugins)

def plotting_engine(config_path: Path, plugins_path: Path) -> None:
    """Plotting engine for VDMTools."""

    config_module = load_module(config_path)
    try:
        plugins = config_module.plugins
    except AttributeError:
        raise AttributeError("Config file must define a `plugins` variable.")

    try:
        plots = config_module.plots
    except AttributeError:
        raise AttributeError("Config file must define a `plots` variable.")

    _plotting_engine(plots, plugins, plugins_path)