from pathlib import Path

import json
import argparse

from .. import cli_standard, _INTERNAL_PLUGINS
from vdmtools import logging


_logger = logging.get_logger(__name__)


main_template = """from typing import Dict, Iterator

import matplotlib.pyplot as plt

from vdmtools.plotting import IStrategyPlugin, CALL_ON_ENTRY, CALL_ON_EXIT

# Add some global matplotlib settings here.
# plt.style.use("classic")
# plt.rcParams["legend.numpoints"] = 1

class GiveMeAName(IStrategyPlugin):
    # Define the iterations attribute here.
    # Make sure to place them in the intened order.
    iterations = [
    #    (name1, filters1),
    #    (name2, filters2),
    #    ...
    ]

    # Define the hook scheme here.
    hook_scheme = {
    #    CALL_ON_ENTRY: {
    #        "iterator": ["method1", "method2"],
    #        ...
    #    },
    #    CALL_ON_EXIT: {
    #        "iterator": ["method1", "method2"],
    #        ...
    #    },
    }

    # Set the unique folder name so users don't have to.
    unique_folder_name = "GiveMeAName"

    # Define the data_description here.
    data_description = None # Good description of the data expected.

    # Define the args_description here.
    args_description = {
    #    "arg1": "description1",
    #    "arg2": "description2",
    #    ...
    }

    # Define the reserved context keys here.
    reserved_context_keys = {
    #    "key1": "description1",
    #    "key2": "description2",
    #    ...
    }

    def __init__(
            self,
            # arg1: type1,
            # arg2: type2,
            **kwargs
        ):
        super().__init__(self, **kwargs)

        # self.arg1 = arg1
        # self.arg2 = arg2

    # Define where the iterators for you plot come from.
    def get_iterators(self, data) -> Dict[str, Iterator[str]]:
        \"\"\"Implement this method\"\"\"

    # If necessary, implement the prepare method.
    # def prepare(self, i, data, context):
    #     # If you wish to call the parent method.
    #     super().prepare(i, data, context)

    # Implement the plot method.
    def plot(self, i, data, context):
        \"\"\"Implement this method\"\"\"

    # If necessary, implement the style method.
    # def style(self, i, data, context):
    #     \"\"\"Implement this method\"\"\"

    # If necessary, implement the save method.
    # def save(self, i, data, context):
    #     # If you wish to call the parent method.
    #     super().save(i, data, context)
"""


def configure_parser(subparsers: argparse._SubParsersAction) -> None:
    subparsers.add_parser(
        "create-plugin",
        help="Create a new plugin",
    )

@cli_standard
def run(args: argparse.Namespace) -> None:
    print("What should we call your plugin?.")
    plugin_name = input("Plugin name: ")

    print("Give a short description of your plugin.")
    plugin_description = input("Plugin description: ")

    print("What should we call your plugin's main module?")
    module_name = input("Module name [default: main]: ") or "main"

    print("Where should we put your plugin?")
    plugins_path = input("Plugins path [default: ./plugins]: ") or "./plugins"

    _logger.info(f"Creating plugin {plugin_name} in {plugins_path}")
    _create_plugin(
        plugin_name,
        plugin_description,
        module_name,
        plugins_path,
    )
    _logger.info(f"Plugin {plugin_name} created successfully")

def _create_plugin(
        plugin_name: str,
        plugin_description: str,
        module_name: str,
        plugins_path: str,
    ) -> None:

    pplugins_path = Path(plugins_path).resolve()

    # Create the plugin directory. If exists, raise an error.
    plugin_path = pplugins_path/plugin_name
    try:
        plugin_path.mkdir(parents=True)
    except FileExistsError:
        raise FileExistsError(f"Plugin {plugin_name} already exists.")

    # Create the __init__.py file in plugin root if it doesn't exist.
    plugins_init = pplugins_path/"__init__.py"
    if not plugins_init.exists():
        _logger.info(f"Creating {plugins_init}")
        with plugins_init.open("w") as f:
            pass

    # Create the __init__.py file in the plugin directory.
    plugin_init = plugin_path/"__init__.py"
    with plugin_init.open("w") as f:
        pass
    _logger.info(f"Created {plugin_init}")

    # Create the main module and plugin.json file.
    with open(plugin_path/f"{module_name}.py", "w") as f:
        f.write(main_template)
    _logger.info(f"Created {plugin_path/f'{module_name}.py'}")

    with open(plugin_path/"plugin.json", "w") as f:
        json.dump({
            "name": plugin_name,
            "description": plugin_description,
            "runtime": module_name,
        }, f, indent=4)
    _logger.info(f"Created {plugin_path/'plugin.json'}")
