import argparse

from vdmtools import logging
from .. import cli_standard


_logger = logging.get_logger(__name__)


def configure_parser(subparser: argparse._SubParsersAction) -> None:
    parser = subparser.add_parser(
        "open-pickle",
        help="Open a previously pickled plot"
    )

    parser.add_argument(
        "path",
        type=str,
        help="Path to the pickle file",
    )

    parser.add_argument(
        "-b", "--backend",
        type=str,
        help="mpl backend to use for plotting",
        default="WebAgg",
        metavar=""
    )

@cli_standard
def run(args: argparse.Namespace) -> None:
    import pickle
    import matplotlib
    import matplotlib.pyplot as plt

    _logger.info(f"Using backend: {args.backend}")
    matplotlib.use(args.backend)

    _logger.info(f"Opening pickle file: {args.path}")
    with open(args.path, "rb") as f:
        fig: plt.figure = pickle.load(f)

    plt.show()