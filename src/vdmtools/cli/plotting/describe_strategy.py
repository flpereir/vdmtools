from typing import Dict
from pathlib import Path

import json
import argparse

from vdmtools import logging
from vdmtools.utils.plugins import load_module
from vdmtools.plotting import IStrategyPlugin
from .. import _INTERNAL_PLUGINS, cli_standard


_logger = logging.get_logger(__name__)


def configure_parser(subparser: argparse._SubParsersAction) -> None:
    parser = subparser.add_parser(
        "describe-strategy", 
        help="Describe the plot strategies available in a plugin"
    )

    parser.add_argument(
        "plugin",
        type=str,
        help="Name of the plugin",
    )

    parser.add_argument(
        "-p", "--plugins",
        help="Path to the plugins directory",
        default=_INTERNAL_PLUGINS,
        metavar=""
    )

@cli_standard
def run(args: argparse.Namespace) -> None:
    # Confirm the config file is a python file
    plugin = Path(args.plugin)

    plugins_path = Path(args.plugins)
    if plugins_path == _INTERNAL_PLUGINS:
        _logger.info("No external plugins directory specified. Using internal plugins.")
    else:
        # Confirm the plugins directory exists
        if not plugins_path.exists():
            raise FileNotFoundError(f"Plugins directory not found: {plugins_path}")

    _describe_strategy(plugin, plugins_path)

def _load_plugins(plugins, plugins_path):
    registered_plugins: Dict[str, type[IStrategyPlugin]]= {}

    for plugin in plugins:
        plugin_path = Path(plugins_path)/plugin

        try:
            with open(plugin_path/"plugin.json", "r") as f:
                plugin_dict = json.load(f)

            plugin_runtime = plugin_dict["runtime"]
        except FileNotFoundError:
            _logger.warning(f"Plugin {plugin} not found.")
            continue
        except KeyError:
            _logger.warning(f"Plugin {plugin} missing `runtime` key.")
            continue

        try:
            module = load_module(plugin_path/f"{plugin_runtime}.py")
        except FileNotFoundError:
            _logger.warning(f"Plugin {plugin} runtime module not found.")
            continue

        try:
            last_loaded = IStrategyPlugin.registered_strategies[-1]
            if last_loaded.__module__ != module.__name__:
                _logger.warning(f"Error loading plugin {plugin}.")
                continue
        except IndexError:
            _logger.warning("Error loading plugin {plugin}.")
            continue

        _logger.info(f"Loaded plugin {plugin}.")
        registered_plugins[last_loaded.__name__] = last_loaded

    IStrategyPlugin.registered_strategies.clear()

    return registered_plugins


def _describe_strategy(plugin: str, plugins_path: Path) -> None:
    registered_plugin = _load_plugins([plugin], plugins_path)

    if len(registered_plugin) == 1:
        strategy = list(registered_plugin.values())[0]
        _logger.info(f"Strategy {strategy.__name__} from plugin {plugin}")
        _logger.info(f"Data description: {strategy.data_description}")
        _logger.info(f"Accepted arguments:")
        for arg, desc in strategy.args_description.items():
            _logger.info(f"\t{arg}: {desc}")
        for arg, desc in strategy.__OPTIONAL_ARGS_DESCRIPTION__.items():
            _logger.info(f"\t{arg}: {desc}")
        _logger.info(f"Reserved context keys:")
        for key, desc in strategy.reserved_context_keys.items():
            _logger.info(f"\t{key}: {desc}")
