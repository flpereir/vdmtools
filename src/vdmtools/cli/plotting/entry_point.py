import argparse

from vdmtools import logging

from . import run_config
from . import describe_strategy
from . import open_pickle
from . import create_plugin


def parser() -> argparse.ArgumentParser:
    """CLI parser for plotting engine."""

    parser = argparse.ArgumentParser(
        description="Plotting engine for VDMTools",
    )

    parser.add_argument(
        "--log-level",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        default="INFO",
        help="Set the log level. Choose from DEBUG, INFO, WARNING, ERROR, CRITICAL",
        metavar=""
    )

    subparsers = parser.add_subparsers(
        dest="subcommand", required=True,
        metavar="SUBCOMMANDS"
    )

    run_config.configure_parser(subparsers)
    describe_strategy.configure_parser(subparsers)
    open_pickle.configure_parser(subparsers)
    create_plugin.configure_parser(subparsers)

    return parser


def main() -> None:
    args = parser().parse_args()

    logging.set_verbosity(logging.verbosity_from_str(args.log_level))

    subcommands = {
        "run-config": run_config.run,
        "describe-strategy": describe_strategy.run,
        "open-pickle": open_pickle.run,
        "create-plugin": create_plugin.run,
    }

    subcommands[args.subcommand](args)
