import argparse

from vdmtools import logging
from . import summary
from . import clone


def parser() -> argparse.ArgumentParser:
    """CLI parser for plugin management."""

    parser = argparse.ArgumentParser(
        description="VDM plugin management",
    )

    parser.add_argument(
        "--log-level",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        default="INFO",
        help="Set the log level. Choose from DEBUG, INFO, WARNING, ERROR, CRITICAL",
        metavar=""
    )

    subparsers = parser.add_subparsers(
        dest="subcommand", required=True,
        metavar="SUBCOMMANDS"
    )
    
    summary.configure_parser(subparsers)
    clone.configure_parser(subparsers)

    return parser

def main() -> None:
    """Main entry point for plugin management."""

    args = parser().parse_args()

    logging.set_verbosity(logging.verbosity_from_str(args.log_level))

    subcommands = {
        "summary": summary.run,
        "clone": clone.run,
    }

    subcommands[args.subcommand](args)