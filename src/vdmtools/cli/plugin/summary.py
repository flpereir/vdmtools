from pathlib import Path

import json
import argparse

from vdmtools import logging
from .. import _INTERNAL_PLUGINS, cli_standard


_logger = logging.get_logger(__name__)


def configure_parser(subparsers: argparse._SubParsersAction) -> None:
    subparser = subparsers.add_parser(
        "summary",
        help="Summarize all existing plugins",
    )

    subparser.add_argument(
        "-p", "--plugins",
        help="Path to the plugins directory",
        default=_INTERNAL_PLUGINS,
        metavar="",
    )

    subparser.add_argument(
        "-v", "--verbose",
        action="store_true",
        help="Show more information about each plugin",
    )

@cli_standard
def run(args: argparse.Namespace) -> None:
    if args.plugins == _INTERNAL_PLUGINS:
        _logger.info("No external plugins specified. Using internal plugins.")

    for plugin_path in Path(args.plugins).iterdir():
        if not plugin_path.is_dir():
            continue
        if plugin_path.stem == "__pycache__":
            continue

        _summarize_plugin(plugin_path, args.verbose)

def _summarize_plugin(plugin_path: Path, be_verbose: bool) -> None:
    try:
        with open(plugin_path/"plugin.json", "r") as f:
            plugin_dict = json.load(f)
    except FileNotFoundError:
        _logger.warning(f"Plugin {plugin_path.name} not found.")
        return

    _logger.info(f"Found Plugin: {plugin_dict['name']}")
    if be_verbose:
        _logger.info(f"\tDescription: {plugin_dict['description']}")
        _logger.info(f"\tRuntime: {plugin_dict['runtime']}")
        _logger.info(f"\tPath: {plugin_path}")
