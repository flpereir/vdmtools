from pathlib import Path

import json
import shutil
import argparse

from vdmtools import logging
from .. import _INTERNAL_PLUGINS, cli_standard


_logger = logging.get_logger(__name__)


def configure_parser(subparsers: argparse._SubParsersAction) -> None:
    subparser = subparsers.add_parser(
        "clone",
        help="Clone an internal plugin to an external directory",
    )

    # at least one plugin is required
    subparser.add_argument(
        "plugins",
        nargs="+",
        help="Name of the plugin(s) to copy",
    )

    subparser.add_argument(
        "destination",
        type=Path,
        help="Path to the destination directory",
    )

def _copy_and_overwrite(from_path: Path, to_path: Path):
    if to_path.exists():
        _logger.warning(f"{to_path} already exists. Overwriting.")
        shutil.rmtree(to_path)
    shutil.copytree(from_path, to_path, ignore=shutil.ignore_patterns("__pycache__"))

@cli_standard
def run(args: argparse.Namespace) -> None:
    if not args.destination.exists():
        _logger.info(f"Creating directory {args.destination}")
        args.destination.mkdir(parents=True)

    for plugin in args.plugins:
        internal_plugin_path = (_INTERNAL_PLUGINS/plugin).absolute()
        external_plugin_path = (args.destination/plugin).absolute()
        if not internal_plugin_path.exists():
            _logger.warning(f"Plugin {plugin} not found.")
            continue

        _logger.info(f"Copying {plugin} to {args.destination}")
        _copy_and_overwrite(internal_plugin_path, external_plugin_path)
