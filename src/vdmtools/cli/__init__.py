from pathlib import Path

from vdmtools import logging
from vdmtools._version import __version__


_INTERNAL_PLUGINS = Path(__file__).parent.parent.resolve() / "plugins"
_logger = logging.get_logger(__name__)

def header(program: str) -> None:
    """Return a header string for the given program."""
    _logger.info(f"""
VdMTools {__version__} COMMAND LINE INTERFACE

Program: {program}
=============================================
""")
    
def footer(program) -> None:
    """Return a footer string."""
    _logger.info(f"""
=============================================
Program: {program}
          
VdMTools {__version__} COMMAND LINE INTERFACE
""")

def cli_standard(func) -> None:
    """Decorator for CLI functions."""
    def wrapper(*args, **kwargs) -> None:
        header(func.__name__)
        func(*args, **kwargs)
        footer(func.__name__)
    return wrapper
