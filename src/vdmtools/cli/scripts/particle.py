import argparse

from vdmtools import logging
from .. import cli_standard


_logger = logging.get_logger(__name__)


def configure_parser(subparsers: argparse._SubParsersAction) -> None:
    subparser = subparsers.add_parser(
        "particle",
        help="Find out the favorite particle of a particle physicist.",
    )

    subparser.add_argument(
        'name',
        type=str,
        help='Name of the particle physicist'
    )

    subparser.add_argument(
        'particle',
        type=str,
        help='Your favorite particle'
    )

@cli_standard
def run(args: argparse.Namespace) -> None:
    _logger.info(f"{args.name}'s favorite particle is {args.particle}. Interesting choice!")
