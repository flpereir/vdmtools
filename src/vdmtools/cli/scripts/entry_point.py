import argparse

from vdmtools import logging
from . import h2b
from . import particle


def parser() -> argparse.ArgumentParser:
    """CLI for script management."""

    parser = argparse.ArgumentParser(
        description="Scripts for VdMTools.",
    )

    parser.add_argument(
        "--log-level",
        choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"],
        default="INFO",
        help="Set the log level. Choose from DEBUG, INFO, WARNING, ERROR, CRITICAL",
        metavar=""
    )

    subparsers = parser.add_subparsers(
        dest="subcommand", required=True,
        metavar="SUBCOMMANDS"
    )
    
    h2b.configure_parser(subparsers)
    particle.configure_parser(subparsers)

    return parser

def main() -> None:
    """Main entry point for plugin management."""

    args = parser().parse_args()

    logging.set_verbosity(logging.verbosity_from_str(args.log_level))

    subcommands = {
        "h2b": h2b.run,
        "particle": particle.run,
    }

    subcommands[args.subcommand](args)