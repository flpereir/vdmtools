from typing import Any, MutableMapping
from collections.abc import MutableMapping as MutableMappingABC

from .strategy import IStrategyPlugin
from vdmtools import logging


_logger = logging.get_logger(__name__)


class VdMPlotter:
    """Skeleton class for all VdM plotting strategies.

    Soul driver of the VdM plotting framework. It delegates all the plotting logic to the
    provided :py:class:`~vdmtools.plotting.IStrategyPlugin` instance. This class is simply
    responsible for providing the loop strucure as specified by the the strategy and calling
    the strategy hooks at the appropriate times.

    .. important::
        From the prespective of this class, the data used for plotting as well as the context
        object are completely opaque. No assumptions are to be made about the data by this class.
        As a results, the implementors of the :py:class:`~vdmtools.plotting.IStrategyPlugin` child
        classes are souly responsible for correctly using the data and context objects.

        .. note::
            The only (reasonable) assumption that can be made about the context object is that it
            implements the :py:class:`~collections.abc.MutableMapping` interface. This is a fancy
            way of saying that the context object must behave like a dictionary (even if it is not
            a pure python dictionary).

    .. important::
        The communication between this class and the strategy is done through the context object.
        As of now, the only context that is set by this class is the current value of all the active
        iterators. The keys for these values are of the form ``current_<iterator_id>`` where
        *iterator_id* is the id of the iterator as specified by the strategy. For example, if the
        strategy specifies an iterator with id ``fits``, then the current value of this iterator
        will be set in the context under the key ``current_fit``.

    Parameters
    ----------
    strategy : :py:class:`~vdmtools.plotting.IStrategyPlugin`
        The strategy that will be used to do the actual plotting. This object is responsible for
        providing the loop structure as well as the hook scheme that will be used by this class.
    """
    def __init__(self, strategy: IStrategyPlugin):
        self.strategy = strategy

    def run(self, data, context: MutableMapping[str, Any] = None):
        """Run the plotting strategy.

        Parameters
        ----------
        data : Any
            The data that will be used for plotting. This object is completely opaque to this class.
            It is up to the strategy to decide how to use this data.
        context : MutableMapping, optional
            The context that will be used for plotting. This object is completely opaque to this
            class apart from the fact that it must implement the :py:class:`~collections.abc.MutableMapping`
            interface. It is also up to the strategy to decide how to use this context.
        """
        if context is None:
            _logger.info("No context provided. Using empty context.")
            context = {}

        # Make sure that contecxt implements the MutableMapping interface
        # This interface requires objects to implement the:
        # * __getitem__ method - used to get a value from the mapping
        # * __setitem__ method - used to set a value in the mapping
        # * __delitem__ method - used to delete a value from the mapping
        # * __iter__ method - used to iterate over the keys in the mapping
        # * __len__ method - used to get the length of the mapping
        if not isinstance(context, MutableMappingABC):
            raise TypeError(f"Context does not implement the MutableMapping interface")

        # VdMPlotter has no idea what 'data' is. It is up to the strategy
        # to decide what loops must be done and what iterators must be used.
        # This is the perfect place for the strategy to do any preparation
        # that it needs to do before the loops start.
        iterators = self.strategy.get_iterators(data)

        # Start the recursion
        _logger.debug("Starting the recursion")
        _logger.debug(f"Iterators: {' '.join(iterators.keys())}")
        self._run(0, data, iterators, context)

    def _run(self, iter_count, data, iterators, context):
        if iter_count == len(self.strategy.iterations):
            # All iterators have been iterated through
            # Nothing to do here but end the recursion
            return

        iterator_id, filters = self.strategy.iterations[iter_count]
        _logger.debug(f"Running iterator {iterator_id}")
        _logger.debug(f"Filters for iterator {iterator_id}: {filters}")

        iterator = iterators[iterator_id]

        # Create the 'current_fit', 'current_detector', or 'current_correction' 
        # key depending on the iterator_id that was passed in
        relevant_context_key = f"current_{iterator_id[:-1]}"

        # Iterato through the iterator and let the plot strategy
        # decide wether to call any of the hooks depending on the
        # current iterator and location in the loop
        _logger.debug(f"Running iterator {iterator_id}")
        for i, value in enumerate(iterator):
            # Apply the filters to the current iterator value. Skip the
            # current iterator value if any of the filters returns True
            if any(map(lambda filter: filter(self.strategy, value, context), filters)):
                _logger.info(f"Value {value} of iterator {iterator_id} was filtered out")
                continue

            # Set the current iterator value in the context
            context[relevant_context_key] = value

            _logger.debug(f"Iteration: {i}")
            _logger.debug(f" Iterator: {iterator_id}")
            _logger.debug(f"    Value: {value}")
            _logger.debug(f"  Context: {context}")

            # Call any hooks that are meant to run on entering the current iterator
            self.strategy.call_on_entry(i, iterator_id, data, context)

            # Recursively call _run for the next iterator
            self._run(iter_count + 1, data, iterators, context)

        # Remove the current iterator value from the context
        # Semantically this value no longer makes sense to be
        # in the context since we are no longer in the loop
        if relevant_context_key in context:
            # The relevant_context_key might not be in the context
            # if the loop was skipped due to a filter
            del context[relevant_context_key]

        # Call any hooks that are meant to run on exiting the previous iterator
        self.strategy.call_on_exit(iterator_id, data, context)
