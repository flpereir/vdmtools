from __future__ import annotations
from typing import Any, List, Dict, Callable, Iterator, Tuple, MutableMapping
from enum import Enum, auto
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt

from vdmtools import logging


_logger = logging.get_logger(__name__)


class HookType(Enum):
    """Hook type enumerations.
    
    This enumeration is used to specify when the hooks should be called. The two possible values are:
    
    * ``CALL_ON_ENTRY``: This value specifies that the hooks should be called when the loop is entered.
    * ``CALL_ON_EXIT``: This value specifies that the hooks should be called when the loop is exited.

    To better ilustrate the difference between the two values, consider the following example:

    .. code-block:: python

        for fit in fits:
            # [1]
            for correction in corrections:
                # [2]
                for detector in detectors:
                    # [3]
                # [4]
            # [5]
        # [6]

    The numbers in the comments are the **entry** and **exit** points of the loops. The following table
    shows when the hooks will be called for each value of the enumeration:


    .. table:: Hook table
        :name: hook table
        :align: center
        

        +------------------+------------------+------------------+
        | HookType         | Iterator         | Call Point       |
        +==================+==================+==================+
        | CALL_ON_ENTRY    | fits             | [1]              |
        +------------------+------------------+------------------+
        | CALL_ON_EXIT     | fits             | [6]              |
        +------------------+------------------+------------------+
        | CALL_ON_ENTRY    | corrections      | [2]              |
        +------------------+------------------+------------------+
        | CALL_ON_EXIT     | corrections      | [5]              |
        +------------------+------------------+------------------+
        | CALL_ON_ENTRY    | detectors        | [3]              |
        +------------------+------------------+------------------+
        | CALL_ON_EXIT     | detectors        | [4]              |
        +------------------+------------------+------------------+

    Note
    ----
    The hooks are called in the order specified in the :py:attr:`IStrategyPlugin.__METHOD_CALL_ORDER__` attribute.
    """
    CALL_ON_ENTRY = auto()
    CALL_ON_EXIT = auto()


class StrategyPluginMount(type):
    """Metaclass that registers all subclasses of `IStrategyPlugin`."""
    registered_strategies: List[type[IStrategyPlugin]] = []

    def __init__(cls, name, bases, attrs):
        super().__init__(cls)
        if name != "IStrategyPlugin":
            _logger.debug(f"Registering strategy {name}")
            StrategyPluginMount.registered_strategies.append(cls)


####################################################################
# This is a type alias for a callable with the following signature:
# Arguments
# ---------
# strategy: IStrategyPlugin
#     This is the strategy that is calling the filter. In python 3.7
#     type aliases cannot be forward referenced so we cannot use the
#     type IStrategyPlugin here. It's set as Any but it actually is
#     an IStrategyPlugin.
# iterator_value: Any
#     This is the current value being iterated over
# context: MutableMapping[str, Any]
#     This is the context that is passed into the plotter
#
# Returns
# -------
# bool
#     True if the current value should be skipped, False otherwise
#
# Note
# ----
# This is used in some strategies to filter out certain values from
# being plotted. For example, when plotting the ratio of a quantity
# between two detectors (given one as a reference), we want to skip
# a detector if its name is the same as the reference.
####################################################################
FilterCallable = Callable[[Any, Any, MutableMapping[str, Any]], bool]

####################################################################
# This is a type alias for a callable with the following signature:
# Arguments
# ---------
# values: np.ndarray
#     This is the array of values that the statistics are being
#     calculated for
# errors: np.ndarray
#     This is the array of errors that the statistics are being
#     calculated for
# context: MutableMapping[str, Any]
#     This is the context that is passed into the plotter
#
# Returns
# -------
# Tuple[float, float]
#     The first element is the expected value and the second
#     element is the error on the expected value
#
# Note
# ----
# This is used to allow the user to specify a custom statistics
# function to be used when calculating the expected value and
# error on the expected value.
####################################################################
StatsCallable = Callable[[np.ndarray, np.ndarray, MutableMapping[str, Any]], Tuple[float, float]]


class IStrategyPlugin(metaclass=StrategyPluginMount):
    """Plugin contract for all VdM plotting strategies.

    This class offers the interface that all VdM plotting strategies adhere to. Strategies are
    handed over to the :py:class:`~vdmtools.plotting.VdMPlotter` class and are responsible for
    intructing the plotter on how to plot the data.

    .. note::
        Communication between the strategy and the plotter is done through the **context** object.
        This object is given to the plotter by the **user** and in turn, the plotter forwards
        it to all the hooks in the strategy.

    Children of this class are given full control over the plotting process. This includes:

    * **The loop structure**: How many loops are there? What are the iterators? What is the order of
      the loops? All of these questions are answered by the strategy via the :py:attr:`iterations`
      attribute.
    * **The hook scheme**: Where and when should the hooks be called? This is specified by the strategy
      via the :py:attr:`hook_scheme` attribute.
    * **The plotting logic**: Is there a preperation step? How should the data be plotted? How should the
      plot be styled? How (and where) should the plot be saved? All of these questions are answered
      by the strategy via the implementation of the :py:meth:`prepare`, :py:meth:`plot`, :py:meth:`style`
      and :py:meth:`save` methods.

    .. important::
        The strategy has **full control** over the plotting process. This also gives it **full responsibility**
        on how to use the data and context objects. The :py:class:`~vdmtools.plotting.VdMPlotter` class
        does not make any assumptions about the data or context objects.

    .. tip::
        Given the enourmous responsibility that the strategy has, each child class should be well documented
        on how to use it. This includes:
        
        * What is the **data**: Is the data a list? A dictionary? A tuple? A numpy array? A pandas dataframe?
          Whatever it is, the developer should document it. This will help the user understand how to use the
          organize the data before passing it to the plotter via the :py:meth:`~vdmtools.plotting.VdMPlotter.run`
          method.
        * What is the **context**: Once again, is the context a plain dictionary? A custom object? This should be clearly
          documented. What additional context is **needed**/**permitted** to be set by the user? This should also be
          documented since it will help the user how to use the construct the context object before passing it to the
          plotter via the :py:meth:`~vdmtools.plotting.VdMPlotter.run` method.

    Attributes
    ----------
    iterations : List[Tuple[str, Iterator[FilterCallable]]]
        Specifies the loop structure of the plotting process.

        Each element in this list represents one loop in the plotting process. A loop is defined by:
        
        * **An iterator id** (str): This is the id of the iterator. It is used to identify the current value
          of the iterator in the context object. For example, if the iterator is ``fits``, then the
          current value of the iterator will be set in the context under the key ``current_fit``.
        * **Filters** (Iterator[FilterCallable]): These filters are used to filter out certain values from the
          iterator. For example, when plotting the ratio of a quantity between two detectors (given one
          as a reference), we want to skip a detector if its name is the same as the reference.

        .. important::
            The **order** of the elements in this list is the order in which the loops will be **executed**.
            The first element will be the outermost loop and the last element will be the innermost loop.

        An example of this attribute is:

        .. code-block:: python

            # This filter assumes the strategy has a 'reference_detector' attribute
            # which is the name of the reference detector for the ratio
            def filter_same_detector(strategy, detector, context):
                return detector == strategy.reference_detector

            iterations = [
                ("fits", []), # No filters
                ("corrections", []), # No filters
                ("detectors", [filter_same_detector]) # One filter
            ]

    hook_scheme : Dict[HookType, Dict[str, Iterator[str]]]
        Specifies when and where the hooks should be called.

        The hook scheme is used to specify where and when the hooks should be called. The keys of the
        dictionary are the :py:class:`HookType` which can be either ``CALL_ON_ENTRY``
        or ``CALL_ON_EXIT``. The values of the dictionary are dictionaries themselves. The **keys** of these
        dictionaries are the **iterator ids** specified in the :py:attr:`iterations` attribute. The **values**
        of these dictionaries are **lists of strings**. These strings are the names of the methods that will
        be called when the hook is called.
    
        For example, the following hook scheme:

        .. code-block:: python
        
            hook_scheme = {
                HookType.CALL_ON_ENTRY: {
                    "corrections": ["prepare"],
                    "detectors": ["plot", "style"]
                },
                HookType.CALL_ON_EXIT: {
                    "detectos": ["save"]
                }
            }

        This scheme specifies that the following hooks will be called:

        * **prepare** and **plot** will be called when the every time the **correction** loop is entered.
        * **plot** and **style** will be called when the every time the **detector** loop is entered.
        * **save** will be called when the **detector** loop is exited.

        Refer to the :ref:`Hook Table <hook table>` for an ilustration of when the hooks will be called.

    unique_folder_name : str
        The unique folder name is used to specify the name of the folder that the plot will be saved in.

        .. tip:: It is recommended that the name of the folder be the name of the strategy class.

    data_description : str
        The strategy data description. Every subclass must implement this attribute and it must describe
        the type of data that the subclass accepts. This is used to help the user understand how to use the
        strategy.

    args_description : Dict[str, str]
        The strategy arguments description. Every subclass must implement this attribute and it must describe
        all the arguments that the subclass accepts in its constructor. 

    reserved_context_keys : Dict[str, str]
        The reserved context keys. Every subclass must implement this attribute and it must describe all the
        keys that the subclass uses in the context object. This is used to prevent the user from overwriting
        keys that the subclass uses.

    Note
    ----
        :py:attr:`iterations`, :py:attr:`hook_scheme` and :py:attr:`unique_folder_name` **must** be set
        by the subclass and cannot be changed via the constructor. This design choice allows for the user
        to now have to worry about setting these attributes when creating a new instance of the subclass.

    Attributes
    ----------
    file_suffix : str
        The suffix that will be appended to the name of the plot file.
    output_folder : Path
        The path to the folder that the plot will be saved in.
    save_pickle : bool
        Whether or not to save the plot as a pickle file.
    """
    __METHOD_CALL_ORDER__ = ["prepare", "plot", "style", "save"]
    __FORCE_ATTR_IMPLEMENTATION__ = [
        "iterations", "hook_scheme", "unique_folder_name",
        "data_description", "args_description", "reserved_context_keys"
    ]
    __OPTIONAL_ARGS_DESCRIPTION__ = {
        "file_suffix": "The suffix that will be appended to the name of the plot file",
        "output_folder": "The path to the folder that the plot will be saved in",
        "save_pickle": "Whether or not to save the plot as a pickle file"
    }

    iterations: List[Tuple[str, Iterator[FilterCallable]]] = None
    hook_scheme: Dict[HookType, Dict[str, Iterator[str]]] = None
    unique_folder_name: str = None
    data_description: str = None
    args_description: Dict[str, str] = None
    reserved_context_keys: Dict[str, str] = None

    def __init__(
            self,
            child: IStrategyPlugin,
            file_suffix: str = "",
            output_folder: Path = Path("plots"),
            save_pickle: bool = False,
        ) -> None:
        self.file_suffix = file_suffix
        self.output_folder = output_folder
        self.save_pickle = save_pickle

        # Validate the child
        self._validate_child(child)

    def get_iterators(self, data) -> Dict[str, Iterator[str]]:
        """Get the iterators for the strategy.

        This method is responsible for returning a dictionary of iterators that will be used by the
        strategy. The keys of the dictionary are the iterator ids as specified in the :py:attr:`iterations`
        attribute. The values of the dictionary are the actual iterators. The way these iterators are obtained
        will depend on the type of **data**. Each subclass is responsible for implementing this method and
        returning the correct iterators.

        Parameters
        ----------
        data : Any
            The data that will be used for plotting. Each subclass is responsible for implementing this method
            and returning the correct iterators.

        Returns
        -------
        Dict[str, Iterator[str]]
            The dictionary of iterators that will be used by the strategy.
        """
        raise NotImplementedError("get_iterators must be implemented by the subclass")

    def prepare(self, i, data, context):
        """Prepare the plot.

        This method is called before the plotting starts. It is responsible for doing any preparation that
        is needed before the plotting starts. The calling of this method is controlled by the :py:attr:`hook_scheme`
        attribute.

        Parameters
        ----------
        i : int
            The index of the current value of the iterator.
        data : Any
            The data used for plotting.
        context : MutableMapping[str, Any]
            The plot context up to this point.

        Note
        ----
        This method is called before the :py:meth:`plot` method.
        """
        plt.clf()

    def plot(self, i, data, context):
        """Plot the data.

        This method is responsible for plotting the data. The calling of this method is controlled by the
        :py:attr:`hook_scheme` attribute.

        Parameters
        ----------
        i : int
            The index of the current value of the iterator.
        data : Any
            The data used for plotting.
        context : MutableMapping[str, Any]
            The plot context up to this point.

        Note
        ----
        This method is called after the :py:meth:`prepare` method and before the :py:meth:`style` method.
        """
        raise NotImplementedError("plot must be implemented by the subclass")

    def style(self, i, data, context):
        """Style the plot.

        This method is responsible for styling the plot. The calling of this method is controlled by the
        :py:attr:`hook_scheme` attribute.

        Parameters
        ----------
        i : int
            The index of the current value of the iterator.
        data : Any
            The data used for plotting.
        context : MutableMapping[str, Any]
            The plot context up to this point.

        Note
        ----
        This method is called after the :py:meth:`plot` method and before the :py:meth:`save` method.
        """
        raise NotImplementedError("style must be implemented by the subclass")

    def save(self, i, data, context):
        """Save the plot.

        This method is responsible for saving the plot. The calling of this method is controlled by the
        :py:attr:`hook_scheme` attribute.

        Parameters
        ----------
        i : int
            The index of the current value of the iterator.
        data : Any
            The data used for plotting.
        context : MutableMapping[str, Any]
            The plot context up to this point.

        Note
        ----
        This method is called after the :py:meth:`style` method.
        """
        path = self._get_output_folder(context)
        path.mkdir(parents=True, exist_ok=True)

        if self.save_pickle:
            self._save_figure_as_pickle(path, plt.gcf())

        plt.savefig(path/f"plot{self.file_suffix}.png")
        _logger.info(f"Plot saved to {path/f'plot{self.file_suffix}.png'}")

    def call_on_entry(self, i, iterator_id, data, context):
        self._call_hook(HookType.CALL_ON_ENTRY, i, iterator_id, data, context)

    def call_on_exit(self, iterator_id, data, context):
        self._call_hook(HookType.CALL_ON_EXIT, -1, iterator_id, data, context)

    def _call_hook(self, hook_type: HookType, i: int, iterator_id: str, data, context):
        # The hooks must be called will be called in the order specified
        # in __METHOD_CALL_ORDER__. Trusting the user to correctly order the
        # hooks in the hook_scheme is not a good idea and its just extra
        # work for the user.
        for method in self.__METHOD_CALL_ORDER__:

            # Check if 'hook_type' is in the hook_scheme. It maynot be if
            # for example the user only wants to call hooks upon entry but
            # not upon exit
            if hook_type not in self.hook_scheme:
                continue

            hook_type_hooks = self.hook_scheme[hook_type]

            # Check if there are any hooks in the hook_scheme for the current iterator_id.
            if iterator_id in hook_type_hooks and method in hook_type_hooks[iterator_id]:
                # Call the hook
                _logger.debug(f"Calling hook {method} with hook_type {hook_type.name} for iterator {iterator_id}")
                getattr(self, method)(i, data, context)

    # TODO: Only precisely validates the 'args_description' attribute. It should valildate them all.
    def _validate_child(self, child: IStrategyPlugin):
        for attr_name in self.__FORCE_ATTR_IMPLEMENTATION__:
            attr = getattr(child, attr_name, None)

            if attr is None:
                raise NotImplementedError(f"The {attr_name} attribute must be implemented by the subclass")

            # If attr is the 'args_description' attribute
            if attr_name == "args_description":
                # Get child's __init__ arguments ignoring 'self' and '**kwargs'
                child_args = child.__init__.__code__.co_varnames[1:-1]

                for arg in child_args:
                    if arg not in attr:
                        _logger.critical(f"{child.__class__.__name__} is missing the description for the '{arg}' argument")
                        _logger.info(f"Existing descriptions: {attr}")
                        raise ValueError(f"{child.__class__.__name__} is missing the description for the '{arg}' argument")

    def _save_figure_as_pickle(self, path: Path, figure: plt.Figure):
        import pickle

        with open(path/f"plot{self.file_suffix}.pickle", "wb") as f:
            pickle.dump(figure, f)

        _logger.info(f"Pickled plot saved to {path/f'plot{self.file_suffix}.pickle'}")

    def _get_output_folder(self, context: Dict[str, Any]) -> Path:
        # Construct the output folder path by using the context and the
        # unique_folder_name
        path = self.output_folder/self.unique_folder_name

        # If the 'current_*' keys are in the context, then add them to the path
        for iterator_id in map(lambda x: x[0], self.iterations):
            relevant_key = f"current_{iterator_id[:-1]}"

            if relevant_key in context:
                path = path/context[relevant_key]

        return path