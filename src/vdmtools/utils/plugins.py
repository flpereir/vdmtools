from types import ModuleType
from pathlib import Path

import importlib.util


def load_module(plugin_path: Path) -> ModuleType:
    """
    Load a plugin module from the given path.

    Parameters
    ----------
    plugin_path : Path
        The path to the plugin module. Must be a Python file.

    Returns
    -------
    ModuleType
        The loaded module.
    """
    # Convert the runtime path to a module name
    plugin_module = plugin_path.as_posix().replace("/", ".")

    # Create a spec from the runtime module
    spec = importlib.util.spec_from_file_location(plugin_module, plugin_path)

    # Create a module from the spec
    module = importlib.util.module_from_spec(spec)

    # Execute the module to populate it
    spec.loader.exec_module(module)

    return module
