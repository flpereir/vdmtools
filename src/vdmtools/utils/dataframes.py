from typing import Tuple

import numpy as np
import pandas as pd


class DataframeUtils:
    """
    Utility class for pandas DataFrames containing VdM scan data.
    """
    @staticmethod
    def filter_by_fit_status(data: pd.DataFrame, fit_status: int) -> pd.DataFrame:
        """
        Filter the given DataFrame by the fit status.

        Parameters
        ----------
        data : pd.DataFrame
            The DataFrame to filter.
        fit_status : int
            The fit status to filter by. Can be one of 0 (good), 4 (bad).

        Returns
        -------
        pd.DataFrame
            The filtered DataFrame.

        Notes
        -----
        The provided DataFrame must contain the columns "fitStatus_X" and "fitStatus_Y".
        """
        return DataframeUtils.filter_by_status(data, "fitStatus", fit_status)

    @staticmethod
    def filter_by_cov_status(data: pd.DataFrame, cov_status: int) -> pd.DataFrame:
        """
        Filter the given DataFrame by the covariance matrix status.

        Parameters
        ----------
        data : pd.DataFrame
            The DataFrame to filter.
        cov_status : int
            The covariance matrix status to filter by. Can be one of 3 (good), 2 (bad).

        Returns
        -------
        pd.DataFrame
            The filtered DataFrame.

        Notes
        -----
        The provided DataFrame must contain the columns "covStatus_X" and "covStatus_Y".
        """
        return DataframeUtils.filter_by_status(data, "covStatus", cov_status)

    @staticmethod
    def filter_by_status(
        data: pd.DataFrame,
        status_column_with_no_plane: str,
        status: int
    ) -> pd.DataFrame:
        """
        Filter the given DataFrame by the status.

        Parameters
        ----------
        data : pd.DataFrame
            The DataFrame to filter.
        status_column_with_no_plane : str
            The name of the column containing the status for the plane with no plane.
        status : int
            The status to filter by. Can be one of 0 (good), 1 (bad).

        Returns
        -------
        pd.DataFrame
            The filtered DataFrame.
        """
        good_bcids_in_x = data[
            data[f"{status_column_with_no_plane}_X"] == status
        ]["BCID"].to_numpy()
        good_bcids_in_y = data[
            data[f"{status_column_with_no_plane}_Y"] == status
        ]["BCID"].to_numpy()

        good_bcids = good_bcids_in_x

        if good_bcids_in_x.shape != good_bcids_in_y.shape or np.any(good_bcids_in_x != good_bcids_in_y):
            good_bcids = np.intersect1d(good_bcids_in_x, good_bcids_in_y)

        data = data[data["BCID"].isin(good_bcids)].reset_index(drop=True)

        assert data[f"{status_column_with_no_plane}_X"].unique() == status
        assert data[f"{status_column_with_no_plane}_Y"].unique() == status

        return data

    @staticmethod
    def match_bcids(
        dataframe1: pd.DataFrame,
        dataframe2: pd.DataFrame
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        """
        Match BCIDs of two DataFrames.

        Parameters
        ----------
        dataframe1 : pd.DataFrame
            First DataFrame.
        dataframe2 : pd.DataFrame
            Second DataFrame.

        Returns
        -------
        Tuple[pd.DataFrame, pd.DataFrame]
            Tuple containing two DataFrames with matching BCIDs in their index.

        Notes
        -----
        This function expects that both DataFrames the BCID values as their index.
        From there, it will return two DataFrames with matching BCIDs.

        Examples
        --------
        >>> import pandas as pd
        >>> df1 = pd.DataFrame({"BCID": [1, 2, 4, 5, 7], "Values": [1, 2, 3, 4, 5]}).set_index("BCID")
        >>> df1
            Values
        BCID        
        1          1
        2          2
        4          3
        5          4
        7          5
        >>> df2 = pd.DataFrame({"BCID": [1, 2, 3, 6, 7], "Values": [1, 2, 3, 4, 5]}).set_index("BCID")
        >>> df2
            Values
        BCID        
        1          1
        2          2
        3          3
        6          4
        7          5
        >>> df1, df2 = match_bcids(df1, df2)
        >>> df1
            Values
        BCID        
        1          1
        2          2
        7          5
        >>> df2
            Values
        BCID
        1          1
        2          2
        7          5
        """
        # Find common indexes
        common_indexes = dataframe1.index.intersection(dataframe2.index)

        # Filter DataFrames based on common indexes
        dataframe1 = dataframe1.loc[common_indexes]
        dataframe2 = dataframe2.loc[common_indexes]

        return dataframe1, dataframe2
