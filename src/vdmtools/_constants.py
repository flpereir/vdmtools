"""
This module contains constants that are used throughout the package.
"""

CORRECTIONS = {
    "noCorr": "NC",
    "BeamBeam" : "BB",
    "Background" : "BG",
    "PeakToPeak" : "P2P",
    "DynamicBeta" : "DB",
    "LengthScale" : "LS",
    "OrbitDriftSep" : "OD1",
    "OrbitDriftRate" : "OD2",
    "ResidualOrbitDrift" : "OD3",
}

LHC_FREQ = 11246
FOUR_NIBBLE = 2**14