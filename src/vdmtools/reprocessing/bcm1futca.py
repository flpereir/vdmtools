from typing import List, Dict

import numpy as np
import pandas as pd

from vdmtools import logging
from vdmtools._constants import LHC_FREQ, FOUR_NIBBLE


_looger = logging.get_logger(__name__)


def calculate_bxraw(
        agghist_df: pd.DataFrame,
        bxmask: np.ndarray,
        channels_to_exclude: List[int],
        per_ch_sigvis: Dict[int, float],
        c0: float = 0.0,
        c2: float = 0.0,
        cutoff: float = 0.8
    ) -> pd.DataFrame:
    """Calculate the bcm1futca rates from the agghist data.

    Parameters
    ----------
    agghist_df : pd.DataFrame
        The agghist data. This dataframe is normally obtained by reading the agghist data from the hd5 file.
    bxmask : np.ndarray
        The bunch crossing collidables mask. Normally obtained from the 'collidable' column of the 'beam' node.
    channels_to_exclude : List[int]
        A list of channels to exclude from the calculation.
    per_ch_sigvis : Dict[int, float]
        A dictionary containing the sig_vis per channel. The keys are the channel ids and the values are the sig_vis.
    c0 : float, optional
        The constant to use in the calculation, by default 0.0
    c2 : float, optional
        The second order correction constant to use in the calculation, by default 0.0
    cutoff : float, optional
        The minimum relative difference between the per channel raw lumi and the reference lumi for a row to be kept, by default 0.8

    Returns
    -------
    pd.DataFrame
        A dataframe containing the same rows as the luminosity tables with the 'bxraw' column added.
    """
    _looger.info('Calculating average sigvis')
    per_ch_sigvis_df: pd.DataFrame = pd.DataFrame(
        per_ch_sigvis.values(), index=per_ch_sigvis.keys(), columns=['sig_vis']
    )
    per_ch_sigvis_df = per_ch_sigvis_df[~per_ch_sigvis_df.index.isin(channels_to_exclude)]
    sigvis_avg = (1 / ((1 / per_ch_sigvis_df).mean())).item()

    agghist_df = agghist_df[agghist_df["channelid"]\
                            .isin(per_ch_sigvis_df.index)]\
                            .reset_index(drop=True)
    agghist_df = agghist_df.merge(per_ch_sigvis_df, left_on="channelid", right_index=True, how="left")

    _looger.info('Computing rate from hits')
    agghist_df["mu"] = (1 - (agghist_df["agghist"] / FOUR_NIBBLE)).apply(
        lambda x: -np.log(x.clip(0))
    )

    _looger.info('Computing per channel raw lumi')
    agghist_df["per_ch_raw_lumi"] = (LHC_FREQ * agghist_df["mu"] / agghist_df["sig_vis"]).apply(
        lambda x: (x*bxmask).sum()
    )
    agghist_df["per_ch_raw_lumi"] = c2 * agghist_df["per_ch_raw_lumi"] ** 2 + agghist_df["per_ch_raw_lumi"] + c0

    _looger.info('Computing reference lumi')
    ref_lumi_df = agghist_df.groupby(["lsnum", "nbnum"]).apply(
        lambda x: np.mean(x["per_ch_raw_lumi"])
    ).reset_index(name="ref_lumi")
    agghist_df = agghist_df.merge(ref_lumi_df, on=["lsnum", "nbnum"], how="left")

    _looger.info(f'Droping rows with per channel raw lumi < {cutoff} * ref lumi')
    good_channel_data = agghist_df[~(agghist_df["per_ch_raw_lumi"] < agghist_df["ref_lumi"] * cutoff)].reset_index(drop=True)

    _looger.info('Computing bxraw')
    good_channel_data["bxraw"] = good_channel_data["mu"] / good_channel_data["sig_vis"]
    bxraw_data = good_channel_data.groupby(["lsnum", "nbnum"]).apply(
        lambda x: np.sum(x["bxraw"]) / len(x["bxraw"]) * sigvis_avg
    ).reset_index(name="bxraw")

    agghist_df = agghist_df.merge(bxraw_data, on=["lsnum", "nbnum"], how="left")\
        .groupby(["lsnum", "nbnum"])\
        .agg({
            "fillnum": "first",
            "runnum": "first",
            "timestampsec": "first",
            "timestampmsec": "first",
            "totsize": "first",
            "publishnnb": "first",
            "datasourceid": "first",
            "algoid": "first",
            "channelid": "first",
            "payloadtype": "first",
            "bxraw": "first"
        }).reset_index()

    return agghist_df
