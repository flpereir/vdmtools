from typing import Dict, Iterator, List, Optional

import numpy as np
import matplotlib.pyplot as plt

from vdmtools import logging
from vdmtools.io import ScanResults
from vdmtools.utils import style, DataframeUtils, TitleBuilder
from vdmtools.plotting import IStrategyPlugin, CALL_ON_ENTRY, CALL_ON_EXIT


_logger = logging.get_logger(__name__)
plt.style.use("classic")
plt.rcParams["legend.numpoints"] = 1


class CorrEffect(IStrategyPlugin):
    # Define the iterations attribute here.
    # Make sure to place them in the intened order.
    iterations = [
        ("fits", []),
        ("corrections", [lambda s, c, ctx: c == "noCorr"]),
        ("detectors", []),
    ]

    # Define the hook scheme here.
    hook_scheme = {
        CALL_ON_ENTRY: {
            "corrections": ["prepare"],
            "detectors": ["plot"],
        },
        CALL_ON_EXIT: {
            "detectors": ["style", "save"],
        },
    }

    unique_folder_name = "CorrEffect"

    data_description = "The data for this plugin must be a ScanResults object."

    args_description = {
        "quantity": "The name of the column to plot.",
        "error": "The name of the column to use as error.",
        "latex": "The latex representation of the quantity.",
        "base_reference": "The base reference correction to use.",
        "fit_status": "The status of the fit to consider. See the ScanResults class for more info",
        "cov_status": "The status of the covariance matrix to consider. See the ScanResults class for more info",
        "fmt": "The format of the plot. For example, 'o' for circles.",
        "colors": "The colors (plt compatible) of the data points.",
        "work_status": "The work status label to use."
    }

    reserved_context_keys = {
        "reference_correction": "The reference correction for the correction effect plot.",
    }

    def __init__(
            self,
            quantity: str,
            error: Optional[str] = None,
            latex: Optional[str] = None,
            base_reference: str = "noCorr",
            fit_status: Optional[str] = "good",
            cov_status: Optional[str] = "good",
            fmt: Optional[str] = "o",
            colors: Optional[List[str]] = ["k", "r", "b", "g", "m", "c", "y"],
            work_status: Optional[str] = "",
            **kwargs
        ):
        super().__init__(self, **kwargs)

        self.error = error
        self.quantity = quantity
        self.latex = latex or quantity
        self.base_reference = base_reference
        self.fit_status = fit_status
        self.cov_status = cov_status
        self.fmt = fmt
        self.colors = colors
        self.work_status = work_status

    def get_iterators(self, data: ScanResults) -> Dict[str, Iterator[str]]:
        # TODO: Use this function to only return iterators that allow the plot to be made without an error.
        return {
            "fits": data.fits,
            "detectors": data.detectors,
            "corrections": data.corrections,
        }

    def prepare(self, i, data: ScanResults, context):
        # The parent method clears the current plt figure.
        super().prepare(i, data, context)

        context["reference_correction"] = self.get_reference_correction(
            correction=context["current_correction"],
            applied_corrections=data.corrections
        )

    def plot(self, i, data: ScanResults, context):
        res = data.filter_results_by(
            context["current_fit"],
            context["current_detector"],
            context["current_correction"],
            self.fit_status,
            self.cov_status
        ).set_index("BCID")
        ref = data.filter_results_by(
            context["current_fit"],
            context["current_detector"],
            context["reference_correction"],
            self.fit_status,
            self.cov_status
        ).set_index("BCID")
        ref, res = DataframeUtils.match_bcids(ref, res)

        if self.error is None:
            _logger.warning(f"No error column specified for {self.quantity}. Assuming error is 0.")
            res_err = ref_err = np.zeros_like(res[self.quantity])
        else:
            res_err = res[self.error]
            ref_err = ref[self.error]

        yaxis = res[self.quantity] / ref[self.quantity]
        yerr = np.abs(yaxis) * np.sqrt(
            (res_err / res[self.quantity]) ** 2
            + (ref_err / ref[self.quantity]) ** 2
        )

        plt.errorbar(
            res.index,
            yaxis,
            yerr=yerr,
            fmt=self.fmt,
            label=context["current_detector"],
            color=self.colors[i],
        )

    def style(self, i, data: ScanResults, context):
        plt.grid(True)
        plt.ticklabel_format(useOffset=False, axis="y")
        style.exp_label(data=True, label=self.work_status, year=data.conditions.scan_time_windows[0][0].year)

        info = f"Effect of {context['current_correction'].split('_')[-1]} on {self.latex}"
        title = TitleBuilder() \
                .set_scan_name(data.scan_name) \
                .set_fit(context["current_fit"]) \
                .set_correction(context["current_correction"]) \
                .set_info(info) \
                .build()

        plt.title(title, y=1.05)
        plt.xlabel("BCID")
        plt.ylabel(info)
        plt.legend(loc="best")

    def get_reference_correction(self, correction: str, applied_corrections: list) -> str:
        """Get the reference correction for the effect plot.
        This is the correction that is applied before the correction
        that is being plotted.

        Parameters
        ----------
        correction : str
            The correction that is being plotted.
        applied_corrections : list
            The list of corrections that are applied to the data.

        Returns
        -------
        str
            The reference correction.
        """
        base_ref_corr = self.base_reference
        keep_looking = True
        tmp_corr = correction
        while keep_looking:
            if len(tmp_corr.split("_")) != 1:
                ref_corr = tmp_corr.replace("_" + tmp_corr.split("_")[-1], "")
            else:
                ref_corr = base_ref_corr
            if ref_corr in applied_corrections:
                keep_looking = False
            else:
                tmp_corr = ref_corr
            if (ref_corr == base_ref_corr) and (base_ref_corr not in applied_corrections):
                raise ValueError(
                    f"""{base_ref_corr} is not in correction dictionary."""
                    """Impossible to make correction effect plot"""
                    f"""Please add {base_ref_corr} to correction dictionary"""
                )

        return ref_corr
