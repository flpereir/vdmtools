from typing import List, Dict, Iterator, Optional

import numpy as np
import matplotlib.pyplot as plt

from vdmtools import logging
from vdmtools.io import ScanResults
from vdmtools.utils import style, TitleBuilder
from vdmtools.plotting import IStrategyPlugin, CALL_ON_ENTRY
from vdmtools.plotting.strategy import StatsCallable


_logger = logging.get_logger(__name__)
plt.style.use("classic")
plt.rcParams["legend.numpoints"] = 1


def calculate_fill_range(data: List[ScanResults]) -> List[int]:
    """
    Calculate the fills that are present in the data.

    Parameters
    ----------
    data : List[ScanResults]
        The list of ScanResults to calculate the fill range from.

    Returns
    -------
    List[int]
        The list of non repeating fills that are present in the data.
    """
    return list({res.conditions.fill for res in data})


class EvolutionPerDetector(IStrategyPlugin):
    iterations = [
        ("fits", []),
        ("corrections", []),
        ("detectors", []),
    ]

    hook_scheme = {
        CALL_ON_ENTRY: {
            "detectors": ["prepare", "plot", "style", "save"],
        },
    }

    unique_folder_name = "EvolutionPerDetector"

    data_description = "The data for this plugin must be a list of ScanResults."

    args_description = {
        "quantity": "The name of the column to plot.",
        "error": "The name of the column to use as error.",
        "latex": "The latex representation of the quantity.",
        "stats_function": "A function that takes the quantity, error and context and returns a tuple with the average and error.",
        "fit_status": "The status of the fit to consider. See the ScanResults class for more info",
        "cov_status": "The status of the covariance matrix to consider. See the ScanResults class for more info",
        "fmt": "The format of the plot. For example, 'o' for circles.",
        "color": "The color (plt compatible) of the data points.",
        "markersize": "The size of the markers.",
        "elinewidth": "The width of the error bars.",
        "avg_places": "The number of decimal places to use for the average value.",
        "err_places": "The number of decimal places to use for the error value.",
        "work_status": "The work status label to use."
    }

    reserved_context_keys = {}

    def __init__(
            self,
            quantity: str,
            error: Optional[str] = None,
            latex: Optional[str] = None,
            stats_function: Optional[StatsCallable] = None,
            fit_status: Optional[str] = "good",
            cov_status: Optional[str] = "good",
            fmt: Optional[str] = "o",
            color: Optional[str] = "blue",
            markersize: Optional[int] = 8,
            elinewidth: Optional[float] = 0.5,
            avg_places: Optional[int] = 2,
            err_places: Optional[int] = 2,
            work_status: Optional[str] = "",
            **kwargs
        ):
        super().__init__(self, **kwargs)

        self.error = error
        self.quantity = quantity
        self.latex = latex or quantity
        self.stats_function = stats_function or (lambda x, y, c: (np.mean(x), np.std(x)))

        # If self.error is None, the per scan stats must not depend on the error
        # to prevent division by zero. using the mean and std as default.
        self._scan_stats = self.stats_function\
                        if self.error is not None\
                        else (lambda x, y, c: (np.mean(x), np.std(x)))

        self.fit_status = fit_status
        self.cov_status = cov_status
        self.fmt = fmt
        self.color = color
        self.markersize = markersize
        self.elinewidth = elinewidth
        self.avg_places = avg_places
        self.err_places = err_places
        self.work_status = work_status

    def get_iterators(self, data: List[ScanResults]) -> Dict[str, Iterator[str]]:
        # TODO: Use this function to only return iterators that allow the plot to be made without an error.
        return {
            "fits": data[0].fits,
            "detectors": data[0].detectors,
            "corrections": data[0].corrections,
        }

    def plot(self, i, data: List[ScanResults], context):
        x_values = np.arange(1, len(data) + 1)
        y_values = np.empty(len(data))
        y_errors = np.empty(len(data))


        for j, result in enumerate(data):
            res = result.filter_results_by(
                context["current_fit"],
                context["current_detector"],
                context["current_correction"],
                self.fit_status,
                self.cov_status,
            )

            if self.error is None:
                _logger.warning(f"No error column specified for {self.quantity}. Assuming error is 0.")
                err = np.zeros_like(res[self.quantity])
                y_values[j], y_errors[j] = self._scan_stats(res[self.quantity], err, context)
            else:
                y_values[j], y_errors[j] = self._scan_stats(res[self.quantity], res[self.error], context)

        plt.errorbar(
            x_values,
            y_values,
            yerr=y_errors,
            fmt=self.fmt,
            color=self.color,
            markersize=self.markersize,
            elinewidth=self.elinewidth,
        )

        avg_places = self.avg_places
        err_places = self.err_places
        avg, err = self.stats_function(y_values, y_errors, context)

        label = f"{self.latex} $=$ {avg:.{avg_places}f} $\\pm$ {err:.{err_places}f} ({err/avg*100:.2f} %)"
        plt.xlim(0, len(data) + 1)
        plt.axhline(avg, color="red", label=label)
        plt.axhline(avg-err, color="orange", linestyle="--", alpha=0.9)
        plt.axhline(avg+err, color="orange", linestyle="--", alpha=0.9)
        plt.fill_between(plt.xlim(), avg-err, avg+err, color="orange", alpha=0.3)

    def style(self, i, data: List[ScanResults], context):
        plt.grid(True)
        plt.ticklabel_format(useOffset=False, axis="y")
        style.exp_label(data=True, label=self.work_status, year=data[0].conditions.scan_time_windows[0][0].year)

        fill_range = calculate_fill_range(data)
        fills_str = "-".join(map(str, fill_range))
        info = f"Evolution of {self.latex}"
        ticks = [res.scan_name or j for j, res in enumerate(data)]

        title = TitleBuilder() \
                .set_scan_name(fills_str) \
                .set_fit(context["current_fit"]) \
                .set_correction(context["current_correction"]) \
                .set_detector(context["current_detector"]) \
                .set_info(info).build()

        plt.title(title, y=1.05)
        plt.xlabel("Scan Names")
        plt.ylabel(info)
        plt.xticks(range(1, len(data) + 1), ticks, rotation=0)
        plt.legend(loc="best")
