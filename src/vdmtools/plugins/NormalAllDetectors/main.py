from typing import Dict, Iterator, List, Optional

import numpy as np
import matplotlib.pyplot as plt

from vdmtools import logging
from vdmtools.io import ScanResults
from vdmtools.utils import style, TitleBuilder
from vdmtools.plotting import IStrategyPlugin, CALL_ON_ENTRY, CALL_ON_EXIT


_logger = logging.get_logger(__name__)
plt.style.use("classic")
plt.rcParams["legend.numpoints"] = 1


class NormalAllDetectors(IStrategyPlugin):
    iterations = [
        ("fits", []),
        ("corrections", []),
        ("detectors", []),
    ]

    hook_scheme = {
        CALL_ON_ENTRY: {
            "corrections": ["prepare"],
            "detectors": ["plot"],
        },
        CALL_ON_EXIT: {
            "detectors": ["style", "save"],
        }
    }

    unique_folder_name = "NormalAllDetectors"

    data_description = "The data for this plugin must be a ScanResults object."

    args_description = {
        "quantity": "The name of the column to plot.",
        "error": "The name of the column to use as error.",
        "latex": "The latex representation of the quantity.",
        "fit_status": "The status of the fit to consider. See the ScanResults class for more info",
        "cov_status": "The status of the covariance matrix to consider. See the ScanResults class for more info",
        "fmt": "The format of the plot. For example, 'o' for circles.",
        "colors": "The colors (plt compatible) of the plot. Must be a list with at least as many elements as the number of detectors.",
        "work_status": "The work status label to use."
    }

    reserved_context_keys = {}

    def __init__(
            self,
            quantity: str,
            error: Optional[str] = None,
            latex: Optional[str] = None,
            fit_status: Optional[str] = "good",
            cov_status: Optional[str] = "good",
            fmt: Optional[str] = "o",
            colors: Optional[List[str]] = ["b", "g", "r", "c", "m", "y", "k"],
            work_status: Optional[str] = "",
            **kwargs
        ):
        super().__init__(self, **kwargs)

        self.error = error
        self.quantity = quantity
        self.latex = latex or quantity
        self.fit_status = fit_status
        self.cov_status = cov_status
        self.fmt = fmt
        self.colors = colors
        self.work_status = work_status

    def get_iterators(self, data: ScanResults) -> Dict[str, Iterator[str]]:
        return {
            "fits": data.fits,
            "detectors": data.detectors,
            "corrections": data.corrections,
        }

    def plot(self, i, data: ScanResults, context):
        res = data.filter_results_by(
            context["current_fit"],
            context["current_detector"],
            context["current_correction"],
            self.fit_status,
            self.cov_status
        )

        if self.error is None:
            _logger.warning(f"No error column specified for {self.quantity}. Assuming error is 0.")
            y_err = np.zeros_like(res[self.quantity])
        else:
            y_err = res[self.error]

        plt.errorbar(
            res["BCID"],
            res[self.quantity],
            yerr=y_err,
            label=context["current_detector"],
            fmt=self.fmt,
            color=self.colors[i]
        )

    def style(self, i, data: ScanResults, context):
        plt.grid(True)
        plt.ticklabel_format(useOffset=False, axis="y")
        style.exp_label(data=True, label=self.work_status, year=data.conditions.scan_time_windows[0][0].year)

        title = TitleBuilder() \
                .set_scan_name(data.scan_name) \
                .set_fit(context["current_fit"]) \
                .set_correction(context["current_correction"]) \
                .set_info(self.latex) \
                .build()

        plt.title(title, y=1.05)
        plt.xlabel("BCID")
        plt.ylabel(self.latex)
        plt.legend(loc="best")
