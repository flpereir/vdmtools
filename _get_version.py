import subprocess

if __name__ == "__main__":
    result = subprocess.run(["python", "src/vdmtools/_version.py"], capture_output=True)
    print(result.stdout.strip().decode())
