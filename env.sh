brildev="srv-s2d16"
lxplus7="lxplus7"
lxplus9="lxplus9"

# Check which machine we are on
# by checking if the env $HOSTNAME
# contains the string defined above
if [[ $HOSTNAME == *"${brildev}"* ]]; then
    echo "Setting up brilconda on ${brildev}"
    brilconda=/nfshome0/lumipro/brilconda3
elif [[ $HOSTNAME == *"${lxplus7}"* ]]; then
    echo "Setting up brilconda on ${lxplus7}"
    brilconda=/cvmfs/cms-bril.cern.ch/brilconda3
elif [[ $HOSTNAME == *"${lxplus9}"* ]]; then
    echo "Setting up brilconda on ${lxplus9}"
    brilconda=/cvmfs/cms-bril.cern.ch/brilconda310
else
    echo "Unknown machine, only ${lxplus9} is supported."
    return
fi

export PYTHONPATH=${brilconda}/lib
export PATH=${brilconda}/bin:$PATH

# Adding HOME/.local/bin to PATH as to fix sphinx-build being called from /usr/bin
# How to check for when using root? Is that a concern?
export PATH=$HOME/.local/bin:$PATH