VdM Tools
#########

The VdM Tools package is a centralised repository of auxiliary tools for the
analysis of the `VdM Framework <https://vdmdoc.docs.cern.ch/index.html>`_ output.


Installation
============

With git
--------

The ``vdmtools`` package can be installed directly from the gitlab repository. To do so, run:

::

   pip install "vdmtools @ git+https://gitlab.cern.ch/flpereir/vdmtools.git"

From source
-----------

The ``vdmtools`` package can be installed in CERN machines as well as in your own machine. Independent of where
you are installing the package, first clone the repository:

::

   git clone https://gitlab.cern.ch/flpereir/vdmtools.git

After cloning the repository, go to the root directory of the package:

::

   cd vdmtools


CERN machines
"""""""""""""

Instalation in CERN machine is to be done by the following steps:

1. Source the ``env.sh`` file in the root directory of the package. This will
   set up the environment for both lxplus7 and lxplus9 machines.

   ::

      source env.sh

   .. note:: This step may suffer changes in the future due to current migration in CERN machines.

   .. todo:: TEST IF THIS STEP IS WORKING FOR BRILDEV

2. Install the package by running using ``pip``:

   ::

      pip install .

   .. note:: To install with developer dependencies, run ``pip install .[dev]``

.. todo:: ADDA THIRD STEP CALLED RUNNING TESTS

Your own machine
""""""""""""""""

To install the package in your own machine, it is best pratice to create a virtual
environment and install the package there. Assuming you are already in a virtual
environment (see `this <https://docs.python.org/3/library/venv.html>`_ for help), you
can install the package by running:

::

   pip install .[local]

.. note:: Developer dependencies can be installed like before.

.. todo:: ADDA THIRD STEP CALLED RUNNING TESTS

Documentation
=============

An effort is being made to document the package. The documentation can be found `here <https://vdmtools.docs.cern.ch/>`_.

Communication
=============

Any issues, bugs, or feature requests can be raised as issues in the `gitlab repository <https://gitlab.cern.ch/flpereir/vdmtools/-/issues>`_.


Contribution
============

The ``vdmtools`` package is under active development. If you want to contribute to the
package, feel free to open a merge request in the `gitlab repository <https://gitlab.cern.ch/flpereir/vdmtools>`_.