from pathlib import Path #  All Paths should be Path objects not strings
from vdmtools.io import read_one_or_many #  Utility to read one or many files into ScanResults

#  A required variable in all configuration files
#  Holds the names of the plugins to load. In this case, only the 'NormalAllDetectors' plugin
plugins = ["MyDetectorRatio"]

#  A required variable in all configuration files
#  A list of dictionaries. Each dictionary represents a type of plot to be generated
plots = [
    {
        "strategy_name": "MyDetectorRatio",
        "strategy_args": {
            "quantity": "CapSigma_X",
            "error": "CapSigmaErr_X",
            "latex": r"$\Sigma_X$", #  The latex representation of the quantity
            "output_folder": Path("MyPlots"), #  Where to save the plots
            "file_suffix": "_CapSigma_X", #  The suffix to add to the file name
            "work_status": "Private Work", #  The work status to add to the plot
        },
        "context": {"fmt": "s"}, #  Give the context a format string to change the marker style
        "data": read_one_or_many( #  Let's read the data in the specified folder.
            Path("../data/8999_no_corr/analysed_data/8999_28Jun23_230143_28Jun23_232943"),
            "VdM1", # The name of the scan
            fits={"DG"} #  The fits to read. In this case, only the DG fit
        )
    },
]