from pathlib import Path #  All Paths should be Path objects not strings
from vdmtools.io import read_one_or_many #  Utility to read one or many files into ScanResults

import numpy as np

#  Creating a custom statistics function
#  This gives the user the control to define how the statistics are calculated
#  In this case, we are using the weighted average and RMS
def statistics(values, errors, context):
    weights = errors ** -2
    avg = np.average(values, weights=weights)
    err = np.sqrt(np.average((values - avg) ** 2, weights=weights))
    return avg, err

#  A required variable in all configuration files
#  Holds the names of the plugins to load. In this case, only the 'EvolutionAllDetectors' plugin
plugins = ["EvolutionAllDetectors"]

#  A required variable in all configuration files
#  A list of dictionaries. Each dictionary represents a type of plot to be generated
plots = [
    {
        "strategy_name": "EvolutionAllDetectors",
        "strategy_args": {
            "quantity": "CapSigma_X",
            "error": "CapSigma_X",
            "latex": r"$\Sigma_X$", #  The latex representation of the quantity
            "output_folder": Path("8999_Fill_plots"), #  Where to save the plots
            "file_suffix": "_CapSigma_X", #  The suffix to add to the file name
            "stats_function": statistics, #  The custom statistics function
        },
        "context": None, #  The context to use. In this case, None means empty context
        "data": read_one_or_many( #  Utilising the glob method of Path objects to read all the scans in the folder
            Path("../data/8999_no_corr/analysed_data/").glob("8999*"),
            ["VdM1", "BI1", "BI2", "VdM2", "VdM3", "VdM4", "VdM5", "VdM6"], # The names of the scans in the order they happened
            fits={"DG"} #  The fits to read. In this case, only the DG fit
        )
    },
]