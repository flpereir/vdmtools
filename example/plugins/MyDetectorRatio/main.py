from typing import Dict, Iterator, Optional

import numpy as np
import matplotlib.pyplot as plt

from vdmtools.io import ScanResults
from vdmtools.utils import style, DataframeUtils, TitleBuilder
from vdmtools.plotting import IStrategyPlugin, CALL_ON_ENTRY, CALL_ON_EXIT

# Add some global matplotlib settings here.
plt.style.use("classic")
plt.rcParams["legend.numpoints"] = 1

class MyDetectorRatio(IStrategyPlugin):
    # Define the iterations attribute here.
    # Make sure to place them in the intened order.
    iterations = [
        ('fits', []),
        ('corrections', []),
        ('detectors', [lambda s, d, ctx: s.reference_detector == d]),
    ]

    # Define the hook scheme here.
    hook_scheme = {
        CALL_ON_ENTRY: {
            "corrections": ["prepare"],
            "detectors": ["plot"],
        },
        CALL_ON_EXIT: {
            "detectors": ["style", "save"],
        },
    }

    # Set the unique folder name so users don't have to.
    unique_folder_name = "MyDetectorRatio"

    data_description = "The data for this plugin must be a ScanResults object."

    # Define the args_description here.
    args_description = {
        "quantity": "The name of the column to plot.",
        "error": "The name of the column to use as error.",
        "latex": "The latex representation of the quantity.",
        "reference_detector": "The detector to use as reference.",
        "work_status": "The work status label to use."
    }

    # Define the reserved context keys here.
    reserved_context_keys = {
        "fit_status": "The status of the fit to consider. See the ScanResults class for more info",
        "cov_status": "The status of the covariance matrix to consider. See the ScanResults class for more info",
        "fmt": "The format of the plot. For example, 'o' for circles.",
        "colors": "The colors (plt compatible) of the data points.",
    }

    def __init__(
            self,
            quantity: str,
            reference_detector: str = "PLT",
            error: Optional[str] = None,
            latex: Optional[str] = None,
            work_status: Optional[str] = "",
            **kwargs
        ):
        super().__init__(self, **kwargs)

        self.error = error
        self.quantity = quantity
        self.latex = latex or quantity # If latex is not provided, use the quantity
        self.reference_detector = reference_detector
        self.work_status = work_status

    def get_iterators(self, data: ScanResults) -> Dict[str, Iterator[str]]:
        return {
            "fits": data.fits,
            "detectors": data.detectors,
            "corrections": data.corrections,
        }

    # If necessary, implement the prepare method.
    # def prepare(self, i, data, context):
    #     # If you wish to call the parent method.
    #     super().prepare(i, data, context)

    def plot(self, i, data: ScanResults, context: dict):
        # Filter the data to get the current detector
        res = data.filter_results_by(
            context["current_fit"],
            context["current_detector"],
            context["current_correction"],
            context.get("fit_status", "good"),
            context.get("cov_status", "good")
        ).set_index("BCID")
        # Filter the data to get the reference detector
        ref = data.filter_results_by(
            context["current_fit"],
            self.reference_detector,
            context["current_correction"],
            context.get("fit_status", "good"),
            context.get("cov_status", "good")
        ).set_index("BCID")
        # Make the BCIDs match between the two dataframes
        # This is necessary since they may differ
        ref, res = DataframeUtils.match_bcids(ref, res)

        # If the error is not provided, assume it is zero
        if self.error is None:
            res_err = ref_err = np.zeros_like(res[self.quantity])
        else:
            res_err = res[self.error]
            ref_err = ref[self.error]

        # Calculate the ratio and propagate the errors
        yaxis = res[self.quantity] / ref[self.quantity]
        yerr = np.abs(yaxis) * np.sqrt(
            (res_err / res[self.quantity]) ** 2
            + (ref_err / ref[self.quantity]) ** 2
        )

        # Plot the data however you want
        plt.errorbar(
            res.index,
            yaxis,
            yerr=yerr,
            fmt=context.get("fmt", "o"),
            label=f"{context['current_detector']}/{self.reference_detector}",
            color=context.get("colors", ["k", "r", "b", "g", "m", "c", "y"])[i],
        )

    # If necessary, implement the style method.
    def style(self, i, data: ScanResults, context):
        plt.grid(True)
        plt.ticklabel_format(useOffset=False, axis="y")
        style.exp_label(data=True, label=self.work_status)

        # Create the title of the plot
        title = TitleBuilder() \
                .set_scan_name(data.scan_name) \
                .set_fit(context["current_fit"]) \
                .set_correction(context["current_correction"]) \
                .set_info(f"Ratio of {self.latex}") \
                .build()

        # Style the plot however you want
        plt.title(title, y=1.05)
        plt.xlabel("BCID")
        plt.ylabel(f"Ratio of {self.latex}")
        plt.legend(loc="best")

    # If necessary, implement the save method.
    # def save(self, i, data, context):
    #     # If you wish to call the parent method.
    #     super().save(i, data, context)
