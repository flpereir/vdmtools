.. VdM Tools documentation master file, created by
   sphinx-quickstart on Tue Dec  5 14:12:08 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to VdM Tools's documentation!
#####################################

The VdM Tools package is a centralised repository of auxiliary tools for the
analysis of the `VdM Framework <https://vdmdoc.docs.cern.ch/index.html>`_ output.


.. contents:: Table of Contents
   :local:
   :depth: 2


.. toctree::
   :maxdepth: 2
   :titlesonly:
   :hidden:

   quickstart/index
   guide/index
   reference/index


Installation
============

With git
--------

The ``vdmtools`` package can be installed directly from the gitlab repository. To do so, run:

::

   pip install "vdmtools @ git+https://gitlab.cern.ch/flpereir/vdmtools.git"

.. note:: To install with developer dependencies, run ``pip install "vdmtools[dev] @ git+https://gitlab.cern.ch/flpereir/vdmtools.git"``

From source
-----------

The ``vdmtools`` package can be installed in CERN machines as well as in your own machine. Independent of where
you are installing the package, first clone the repository:

::

   git clone https://gitlab.cern.ch/flpereir/vdmtools.git

After cloning the repository, go to the root directory of the package:

::

   cd vdmtools


CERN machines
"""""""""""""

Instalation in CERN machine is to be done by the following steps:

1. Source the ``env.sh`` file in the root directory of the package. This will
   set up the environment for both lxplus7 and lxplus9 machines.

   ::

      source env.sh

   .. note:: This step may suffer changes in the future due to current migration in CERN machines.

   .. todo:: TEST IF THIS STEP IS WORKING FOR BRILDEV

2. Install the package by running using ``pip``:

   ::

      pip install .

   .. note:: To install with developer dependencies, run ``pip install .[dev]``

.. todo:: ADDA THIRD STEP CALLED RUNNING TESTS

Your own machine
""""""""""""""""

To install the package in your own machine, it is best pratice to create a virtual
environment and install the package there. Assuming you are already in a virtual
environment (see `this <https://docs.python.org/3/library/venv.html>`_ for help), you
can install the package by running:

::

   pip install .[local]

.. note:: Developer dependencies can be installed like before.

.. todo:: ADDA THIRD STEP CALLED RUNNING TESTS


.. _vdmtools-usage:

Commands
========

The ``vdmtools`` package provides a number of command line tools which can
be used to perform various tasks. These tools are prefixed with ``vdm``.

.. _vdmplugin-tool:

Plugin Tool - ``vdmplugin``
---------------------------

The ``vdmplugin`` tool is used for everyting plugins related. This tool is composed of
subtools that help with the management of plugins in their own right.

::

   $ vdmplugin --help

The output will be:

.. literalinclude:: _static/cli/generated/vdmplugin.txt
   :language: console

.. _vdmplot-tool:

Plotting Tool - ``vdmplot``
---------------------------

The ``vdmplot`` provides the entry point for using the plot plugins. It's from this
tool that the user take advantage of the plotting plugins that are available.

::

   $ vdmplot --help

The output will be:

.. literalinclude:: _static/cli/generated/vdmplot.txt
   :language: console

Scripts Tool - ``vdmscript``
----------------------------

The ``vdmscripts`` tool is used to run scripts that are available in the package. These
scripts are used to perform general tasks.

::

   $ vdmscripts --help

The output will be:

.. literalinclude:: _static/cli/generated/vdmscripts.txt
   :language: console

Communication
=============

Any issues, bugs, or feature requests can be raised as issues in the `here <https://gitlab.cern.ch/flpereir/vdmtools/-/issues>`_.


Contribution
============

The ``vdmtools`` package is under active development. If you want to contribute to the
package, feel free to open a merge request in the `gitlab repository <https://gitlab.cern.ch/flpereir/vdmtools>`_.