Plotting
########

The generation of plots is done with ease via a **plugin system**. If a plot plugin exists, only a single
configuration file is needed to generate the plots for such plugin. This quickstart will address how to use
the existing plugins as well as how to start creating your own plugins.


.. contents:: Table of Contents
   :local:
   :depth: 2


.. note:: 
    The configuration language was chosen to be **python** due to its simplicity and the fact that it is a full
    programming language. This allows for a lot of flexibility and the possibility to configure files in a very
    compact way (as opposed to **YAML** or **JSON** where you need to repeat a lot of information).

.. _using-an-existing-plugin:


Learning about the existing plotting plugins
============================================

The builtin plugins are located in the ``plugins`` directory in the root of the project. The plugins are can be inspected
by looking at the ``main.py`` file in each plugin directory. Alternatively, you can use the :ref:`vdmplot <vdmplot-tool>` cli
tool to get a description of what the plot accepts as input. For example, running the following command:


::

    $ vdmplot describe-strategy DetectorRatio

should give you the following output:

.. literalinclude:: ../_static/cli/generated/vdmplot_describe_strategy.txt
    :language: console

Using an existing plugin
========================

As stated above, the bridge between the plugins and the plotting subpackage is done via a configuration file.
As an examle, let us consider the plugin **NormalAllDetectors** present in the **plugins** folder in the root directory of
the project.

Inspecting the ``main.py`` file in the directory, we can see the classe ``NormalAllDetectors``.

A configuration file that uses the ``NormalAllDetectors`` class would look like this:

.. literalinclude:: ../../../example/configs/plotting_config_1.py
    :language: python

.. seealso:: 

    For more information see the documentation on the :py:func:`~vdmtools.io.read_one_or_many` function.

Once the configuration file is ready, we can generate the plots by running the builtin cli tool :ref:`vdmplot <vdmplot-tool>` by 
providing it with the **path to the configuration file** as well as the **path to the plugins directory** where the plugin
we want to use is located.

::

    $ vdmplot config.py plugins/

The ``vdmtools`` will keep you updated on the progress of the plot generation and will let you know when it is done.

Once the plots are generated, they will be saved in the folder specified in the configuration file with a easy to
follow folder structure. For the example above, these are some of the plots that are generated:

.. container:: image-row

   .. image:: ../_static/plots/generated/NormalAllDetectors/DG/noCorr/plot_CapSigma_X.png
      :width: 450px

   .. image:: ../_static/plots/generated/NormalAllDetectors/DG/BeamBeam_DynamicBeta/plot_CapSigma_X.png
      :width: 450px

.. note:: BCM1FUTCA is missing from the plots because it is not present in the path I provided.

Next follows a configuration file that uses the the ``EvolutionAllDetectors`` plugin:

.. literalinclude:: ../../../example/configs/plotting_config_2.py
    :language: python

.. important:: 

    There are a few things to note in this configuration file:

    * First, notice how little the configuration file changed. Using a different plugin ia as easy as changing the name of the
      plugin in the ``plugins`` variable and changing the name of the strategy in the ``strategy_name`` variable.
    * Second, notice how we are using the :py:func:`~vdmtools.io.read_one_or_many` function to read all the scans in a folder. This utility functions
      , as the name suggests, supports reading a single results folder or many.
    * Third, notice how we are using a **custom statistics function** to calculate the statistics of the quantity in whatever way we
      want. This is done by passing the function to the ``stats_function`` argument of the plugin.

Running the :ref:`vdmplot <vdmplot-tool>` cli again on this configuration file will generate plots of the following form:

.. container:: image-row

   .. image:: ../_static/plots/generated/EvolutionAllDetectors/DG/noCorr/plot_CapSigma_X.png
      :width: 450px

   .. image:: ../_static/plots/generated/EvolutionAllDetectors/DG/BeamBeam_DynamicBeta/plot_CapSigma_X.png
      :width: 450px

.. _creating-a-new-plugin:

Creating a new plugin
=====================

It may be the case that the plot you want is not present in the existing plugins. In this case, you can create your own plugin
and use it in the same way as the builtin plugins.

The bes  way to create a new plugin is to use the :ref:`vdmplugin <vdmplugin-tool>` cli tool. This tool manages all thins related to plugins. We can
use it to create a new plugin by running the following command:

::

    $ vdmplugin create

Upon running this command, the creation of the plugin will take place in an interactive manner. The tool will ask you the name of
the plugin, the description amongst other things. After going through the interactive process, go to the location you specified
and you will find a folder containing the folloing:

* A **python** file (by default named **main.py**) where you will write the code for the plugin. There will be a template class in the
  file that you can use as a starting point.
* A **JSON** file that contains the metadata of the plugin.

Creating the plugin now requires some understanding on how the children of :py:class:`~vdmtools.plotting.IStrategyPlugin` interact with the 
:py:class:`~vdmtools.plotting.VdMPlotter`. For a full example, please refer to the :ref:`plotting-example` section in the :ref:`plotting-guide`.


.. _adding-a-new-plot:

Adding a new plugin to the builtin plugins
==========================================

.. important::

    The addition of new plugins to the builtin plugins should be done with caution. You should only add a plugin to the builtin plugins
    after making sure that it is well tested and documented.

If you want to add your plugin to the builtin plugins, you can do so by creating simply copying the plugin folder to the ``plugins``
directory in the root of the project.

.. note::

    Only copy the folder of the plugin you want to add. Do not copy the whole ``plugins`` directory.

After, create a pull request to the ``master`` branch of the project and wait for it to be reviewed and merged.