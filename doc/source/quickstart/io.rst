IO
##

The IO subpackage provides a set of utilities for working with all kinds of
files that are used in the context of the VdM analysis.

.. contents:: Table of Contents
    :depth: 2
    :local:


Reading the results from the VdM Framework
==========================================

The VdM Framework produces a directory of results, normally with the structure
``<framework_output dir>/analysed_data/<scan_id>``. The ``scan_id`` is the familiar
``<fill_number>_<start_time>_<end_time>`` folder name. This folder contains all the results 
from the run, including:

* Run conditions (in the ``cond`` folder)
* Per detector, correction and fit results (following the ``<detector>/results/correction/`` folder structure)

The following code block shows how to read the results from the framework output:

.. code-block:: python

    from pathlib import Path
    from vdmtools.io import ScanResults

    # Path to the framework output directory. All the way to the <scan_id> folder.
    framework_output_dir = Path("/path/to/framework/output/analysed_data/<scan_id>")

    # Create a ScanResults object from the framework output directory
    scan_results = ScanResults(framework_output_dir)

    print(scan_results)

The output should look something like ::

    ScanResults from: /path/to/framework/output/analysed_data/<scan_id>
    Scan name: <scan-name>
    Scan ID: <scan-id>
    ScanConditions:
            Fill: <fill-number>
            Run: (<run-number>, <run-number>)
            Scan names: ('<scan-name>', '<scan-name>')
            Scan types: ('<scan-type>', '<scan-type>')
            Scan time windows:
                    First  Scan: <start-time> - <end-time>
                    Second Scan: <start-time> - <end-time>
            Beta star: <beta-star>
            Angle: <angle>
            Offset: [<offset>, <offset>]
            Particle type B1: <ptype>
            Particle type B2: <ptype>
            COM energy B1 (GeV): <energy>
            COM energy B2 (GeV): <energy>

    Results:
            Fits: <set-of-fits-read>
            Detectors: <list-of-detectors-read>
            Corrections: <set-of-corrections-read>

It is also possible to read the results by using the :py:func:`~vdmtools.io.read_scan_results.read_one_or_many` function:

.. code-block:: python

    from pathlib import Path
    from vdmtools.io import read_one_or_many

    path = Path("/eos/user/f/flpereir/www/UpdateOnGS8999/8999_no_corr/analysed_data/8999_28Jun23_230143_28Jun23_232943")
    scan = read_one_or_many(path, "VdM1")

    print(scan)

For the particular result path I gave, the output would be:

::

    ScanResults from: /eos/user/f/flpereir/www/UpdateOnGS8999/8999_no_corr/analysed_data/8999_28Jun23_230143_28Jun23_232943
    Scan name: VdM1
    Scan ID: 8999_28Jun23_230143_28Jun23_232943
    ScanConditions:
            Fill: 8999
            Run: (369802, 369802)
            Scan names: ('X1', 'Y1')
            Scan types: ('NormalX', 'NormalY')
            Scan time windows:
                    First  Scan: 2023-06-28 23:01:43 - 2023-06-28 23:17:35
                    Second Scan: 2023-06-28 23:29:43 - 2023-06-28 23:45:35
            Beta star: 19.2
            Angle: 0.0
            Offset: [0.0, 0.0]
            Particle type B1: proton
            Particle type B2: proton
            COM energy B1 (GeV): 6799
            COM energy B2 (GeV): 6799

    Results:
            Fits: {'DG', 'SG', 'Poly4G'}
            Detectors: ['BCM1F', 'HFET', 'HFOC', 'PLT']
            Corrections: {'noCorr', 'BeamBeam_DynamicBeta'}

Filtering the results
---------------------

After obtaining your :py:class:`~vdmtools.io.ScanResults` object, you can filter the results using the
:py:meth:`~vdmtools.io.ScanResults.filter_results_by` method as follows:

.. code-block:: python

    dataframe = scan_results.filter_results_by("SG", "PLT", "noCorr")
    print(dataframe)

The output would be a pandas DataFrame with the fit and luminosity calibration results
for SG fit, PLT detector and no correction applied.

You may also provide additional filters like ``fit status`` or ``covariance matrix status``:

.. code-block:: python

    dataframe = scan_results.filter_results_by("SG", "PLT", "noCorr", fit_status="good", cov_status="good")
    print(dataframe)

The output would now only contain the columns of the results that contain the fit and cov status that are
"good" (for fit is 0 and for cov is 3).

.. seealso::

    To learn more about the :py:class:`~vdmtools.io.ScanResults` and all the functions in the :py:mod:`~vdmtools.io` package,
    please refer to the documentation of each class and function.
