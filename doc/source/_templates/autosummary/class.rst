{% extends "!autosummary/class.rst" %}
{#
Ignore the "ignored_methods" methods.
#}

{% block methods %}
   {# Define a list of methods to ignore #}
   {% set ignored_methods = ["__init__", "call_on_entry", "call_on_exit"] %}

   {# Filter out ignored methods #}
   {% set methods = methods | reject("in", ignored_methods) | list %}
   {% if methods %}
   .. rubric:: Methods

   .. autosummary::
   {% for item in methods %}
      ~{{ name }}.{{ item }}
   {%- endfor %}
   {% endif %}

{% endblock %}

{% block attributes %}
  {% if attributes %}
  {% endif %}
{% endblock %}
