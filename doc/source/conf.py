# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import sys
import pathlib
sys.path.insert(0, (pathlib.Path(__file__).parents[2].resolve() / 'src').as_posix())


# -- Project information -----------------------------------------------------

project = 'VdM Tools'
copyright = '2023, Fábio Carneiro'
author = 'Fábio Carneiro'

# The full version, including alpha/beta/rc tags
release = '0.1.0'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.todo', # Support for todo lists
    'sphinx.ext.autodoc', # Support for automatic documentation generation
    'sphinx.ext.napoleon', # Support for Google-style docstrings
    "sphinx.ext.autosummary", # Support for automatic summary generation
]
exclude_patterns = []

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

autosummary_generate = True
autodoc_default_options = {
    "members": False,
    "undoc-members": False,
    "inherited-members": True,
    "autodoc_member_order": "groupwise",
    # Add any member that should not be documented by autodoc here
    "exclude-members": """
        call_on_entry, call_on_exit,
        unique_folder_name, iterations, hook_scheme, args_description, reserved_context_keys, data_description,
        StrategyPluginMount
    """,
    "noindex": True,
}

# -- Configuration for todo extension ----------------------------------------
# https://www.sphinx-doc.org/en/master/usage/extensions/todo.html#configuration

# Do not forget to comment the following line before commiting
# todo_emit_warnings = True


# -- Options for HTML output -------------------------------------------------


# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
html_theme_options = {
    "navigation_with_keys": True,
    'sticky_navigation': True, # Make the navigation bar sticky
    'navigation_depth': 3, # Depth of the navigation tree
}
html_css_files = [
    "css/custom.css",
    "css/basic.css",
]
