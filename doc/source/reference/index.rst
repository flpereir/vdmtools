API Reference
=============

.. toctree::
    :maxdepth: 1

    io
    plotting
    cli
    logging
    utils
