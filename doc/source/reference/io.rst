.. module:: vdmtools.io

IO
==

The :mod:`~vdmtools.io` package provides an API for reading the results of the `VdM Framework <https://vdmdoc.docs.cern.ch/index.html>`_.

.. autosummary::
   :toctree: generated/
   :nosignatures:

   vdmtools.io.ScanResults
   vdmtools.io.read_one_or_many
   vdmtools.io.read_scan_conditions
