.. module:: vdmtools.plotting

Plotting
========

The :mod:`~vdmtools.plotting` package provides an easy interface to creating plots while hiding all the underlying boilerplate code.

.. autosummary::
   :toctree: generated/
   :nosignatures:

   vdmtools.plotting.VdMPlotter
   vdmtools.plotting.HookType
   vdmtools.plotting.IStrategyPlugin