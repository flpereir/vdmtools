.. module:: optuna.cli

Command Line Programs
=====================

The :mod:`~optuna.cli` package implements VdMTools' command-line functionalities.

Currently, the following commands are

.. code-block:: console

    $ vdmplugin --help

and

.. code-block:: console

    $ vdmplot --help

.. seealso::
    The :ref:`vdmtools-usage` section for more details.