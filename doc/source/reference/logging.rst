.. module:: vdmtools.logging

Logging
=======

The :mod:`~vdmtools.logging` module implements logging using the Python ``logging`` package. Library users may
be especially interested in setting verbosity levels using :func:`~vdmtools.logging.set_verbosity` to one of 
``vdmtools.logging.CRITICAL`` (aka ``vdmtools.logging.FATAL``), ``vdmtools.logging.ERROR``, ``vdmtools.logging.WARNING`` 
(aka ``vdmtools.logging.WARN``), ``vdmtools.logging.INFO``, or ``vdmtools.logging.DEBUG``.


.. autosummary::
   :toctree: generated/
   :nosignatures:

   vdmtools.logging.get_verbosity
   vdmtools.logging.set_verbosity
   vdmtools.logging.disable_default_handler
   vdmtools.logging.enable_default_handler
   vdmtools.logging.disable_propagation
   vdmtools.logging.enable_propagation